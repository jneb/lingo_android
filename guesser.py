#!python2.7
"""Background process for lingo that guesses words
uses the guessing engine from lingo.py

Do a request to the guess by assigning to message.
This is picked up by processMessage. Messages are:
    an ndarray with dtype=coloredLetter (from grid.py):
        Guess given these words and replies
    "forget":
        Guess different word. Word is removed from word list in guesser only
    "restart <n>" (where <n> is a number)
        reinitialize guesser for n-character words
        (but you can send a request before it is ready)
    "stop":
        Stop the background task
    'correct':
        rebuild corrections file
    'update':
        reset lingo settings from config

results are passed via these properties, which are thread safe to read:
reply: the last guessed word
status: a short string describing what is going on (for display)
thoughts: a multiline string explaining the guessing process

When any of these changes, a callback is done to the main thread
with the name of the property

When creating the Guesser object, you pass
the application config and the callback routine to call.
Each of these is only every accessed in the foreground to prevent race conditions.
The user is kept updated with status and optionally thoughts
- Selecting starting letter  or position
 a- Busy reading: S"Inlezen"
 b- Ready: S"%d woorden"
- First guess:
 c- List read: S"Selecteren"
 d- else S"Inlezen"
 e- Then: S"%d woorden"
- Subsequent guesses:
 f- S"Zoeken" (up to three times)
 g- S"%d woorden"
-h Final guess is two words: S"laatste gok"
-i No solution: S"onbekend"

Thoughts list contains
0a 'Inlezen woorden van %d letters'
 b 'Er zijn %d woorden van %d letters'
1cd 'Inlezen/zoeken woorden die voldoen aan %s'
 e '%d woorden voldoen aan %s'
2f '[%d ]mogelijke woorden:'
3f samenvatting of lijst
4f 'Mogelijke woord "%s" geeft Q = %f'
 h 'Er %s maar %d woord%s, dus ik gok'
5f 'Passende woord "%s" geeft Q = %f'
6f '"%s" geeft Q = %f'
7i "Voer het woord in"
 j "Je hebt een fout gemaakt"
 j ""%s" is ingevoerd in woordenlijst"
"""

from kivy.clock import Clock
from kivy.logger import Logger
from threading import Thread, Lock, Event

import json
from time import sleep, time as clock
import os
import numpy
from random import randrange
from itertools import islice, chain

from grid import BLUE, RED, YELLOW
import lingo
from lingo import string2woordenA, woordenA2strings, letter2index, index2letter
from lingo import strings2woordenA, letterlijst as letterList

# moods: in status[1]
HAPPY, BUSY, PROBLEM = range(3)
# length of thoughts text
LINELENGTH = 50
# high value for margin check
NEVER = 10

class Interrupt(Exception):
    """Internal exception for interruption of dictionary reading
    """

class Guesser(Thread):
    """Redesign
    rader: The WoordRader for this size
        ideally WoordRader("." * len(self.data.shape[1]))
        check rader.patroon to see
        but may use a more limited pattern or even be None
    note that rader maybe killed unexpectedly by calling process
    if rader is not None,
        rader.woordenA contains all words matching rader.patroon
    pattern: intial pattern of first line (dots and letters)
    patternA: words matching pattern, subset of rader.woordenA
    data: guesses and colors
    thoughts: reasoning behind last guess
    lock: for accessing properties between the foreground and background
        message, reply, status, thoughts
    changed: event to tell guesser it has to do something
    """
    def __init__(self, callback, config):
        super(Guesser, self).__init__()
        self.callback = callback
        self.config = config
        # for process control
        self.lock = Lock()
        self.changed = Event()
        # communication properties: message, reply, status, thoughts
        self._reply = None
        self._message = ""
        self.status = "Opstarten", BUSY
        self.clearThoughts()

        self.rader = self.data = self.pattern = None
        # rader global variables concerning the dictionaries
        self.folder = self.files = self.exceptions = None

    def __repr__(self):
        if not isinstance(self.data, numpy.ndarray): return 'rader'
        woorden = ' '.join(map(''.join, self.data['letter']))
        raderDesc = self.rader and "[{:s}->{:d}, {:d}]".format(
            self.rader.patroon,
            len(self.rader.woordenA),
            len(self.rader.mogelijkA))
        selfDesc = "[{:s}->{:d}]".format(
            self.pattern,
            (len(self.patternA) if hasattr(self, 'patternA') else 0),
            )
        return 'rader("{:s}", guesser={:s}, rader={:s})'.format(
            woorden,
            selfDesc,
            raderDesc,
            )

    # external interface-----------------------
    # communication properties, thread safe
    # changing message (incoming) causes changed to be set
    # changing the outgoing properties (reply, status, thoughts) causes callback
    @property
    def message(self):
        """command from main thread
        string, or ndarray
        """
        with self.lock:
            return self._message

    @message.setter
    def message(self, value):
        if self.changed.isSet():
            # wait until previous message is processed
            # this almost never occurs
            Logger.warning("{:5.2f} message {} delayed by {}".format(
                clock() % 60,
                value,
                self._message))
            while self.changed.isSet():
                sleep(.1)
        if isinstance(value, numpy.ndarray):
            value = value.copy()
        with self.lock:
            self._message = value
            # notify background process
            self.changed.set()

    @property
    def reply(self):
        """reply from guesser thread
        string
        """
        with self.lock:
            return self._reply

    @reply.setter
    def reply(self, value):
        with self.lock:
            self._reply = value
            # Notify foreground process
            Clock.schedule_once(
                lambda ev: self.callback("reply"))

    @property
    def status(self):
        """status of guesser
        string
        """
        with self.lock:
            return self._status

    @status.setter
    def status(self, value):
        with self.lock:
            self._status = value
            # Notify foreground process
            Clock.schedule_once(
                lambda ev: self.callback("status"))

    def setStatusWoorden(self, possibleA):
        """Given an array of possibilities, update status with text like:
            23 woorden
        and handles 0 and 1 case properly
        """
        lenPossible = len(possibleA)
        if lenPossible:
            self.status = "{} woord{}".format(
                lenPossible,
                ('' if lenPossible == 1 else 'en')), HAPPY
        else:
            self.status = "Kan niet", PROBLEM

    @property
    def thoughts(self):
        """description of reasoning
        returned as string
        """
        with self.lock:
            return '\n'.join(filter(None, self._thoughts))

    def clearThoughts(self, lo=0):
        """Erase thoughts from given line.
        Does not send an update;
        you are probably overwriting one of these lines soon.
        """
        # initialize thoughts for default input
        if lo == 0: self._thoughts = []
        assert len(self._thoughts) == 8
        self._thoughts[lo:] = [''] * (8 - lo)

    def setThoughts(self, line, value):
        """List of lines that describe the searching process.
        See top op file.
        Missing lines are empty strings
        """
        with self.lock:
            self._thoughts[line] = value
        # Notify foreground process
        Clock.schedule_once(
            lambda ev: self.callback("thoughts"))

    def shutdown(self):
        """Stop the background process
        """
        self.clearThoughts()
        Logger.debug("{:5.2f} Guesser.shutdown()".format(clock () % 60))
        self.message = "stop"
        self.join(1)
        if self.isAlive():
            Logger.info("Status: {:5.2f} Wil niet stoppen...".format(
                         clock() % 60))

    # message processing---------------------------------
    def run(self):
        """main loop
        """
        Logger.info("{:5.2f} Guesser.run starting".format(
            clock() % 60,
            ))
        self.update()
        self.keepRunning = True
        try:
            while self.keepRunning:
                # allow other thread
                sleep(1e-3)
                # wait for something to happen, then do it
                self.changed.wait()
                # fetch message, acknowledge reception
                message = self.message
                self.changed.clear()
                self.processMessage(message)
        except Exception:
            import traceback
            Logger.error("{:5.2f}: Background proces crashed:\n{}".format(
                clock() % 60,
                traceback.format_exc()))
            raise
        finally:
            self.status = 'Gestopt', PROBLEM

    def processMessage(self, message):
        """gets message put here by update
        message is array with data,
        or string verb with digits
        """
        messageTable = ('stop', 'restart', 'forget', 'correct', 'update', 'dump')
        if isinstance(message, numpy.ndarray):
            self.data = message
            guessFlag = True
        elif message[:7] in messageTable:
            getattr(self, message[:7])(message[7:])
            guessFlag = message == 'forget'
        else:
            Logger.error('{:5.2f} processMessage: invalid message "{}"'.format(
                clock() % 60,
                message))
            return
        if guessFlag:
            # in case of new challenge, or 'forget': start guessing
            # guessers are responsible for putting answer in reply
            # and do more stuff if they want to
            # this is modified by computeSets if applicable
            self.possibleWords = None
            pattern = ''.join(self.data[0]['letter'])
            try:
                if '.' in pattern:
                    self.initialGuess(pattern)
                else:
                    self.guess()
            except Interrupt as e:
                self.status = e.args[0], PROBLEM
                return
            self.generaliseDisplay()

    def generaliseDisplay(self):
        """replace the status by the "official" spelling of the word,
        and if applicable, replace list of possible words
        show words that are not accepted by settings separately
        """
        reply = self.reply
        chosenWord = reply[0]
        # possibleWords is set if getPossible writes line 3 with options
        # and handled by generaliseDisplay
        possibleWords = [chosenWord] + (self.possibleWords or [])
        try:
            generalisedList = self.generaliseList(possibleWords)
        except Interrupt:
            return
        generalised = generalisedList[chosenWord]
        if generalised is not None and len(generalised) == 1 and generalised[0] != chosenWord:
            self.status = generalised[0], HAPPY
        if self.possibleWords:
            self.setThoughts(3,
                ','.join(chain.from_iterable(generalisedList[w]
                                             for w in self.possibleWords)))

    def generaliseList(self, wordlist):
        """Given a list of canonical words,
        produce a mapping of these words to their generalised counterparts.
        The words may map to multiple words, so it is a dictionary
        from words to lists.
        If a word is not found, it maps to None
        """
        Logger.debug("generaliseList "+str(wordlist))
        result = dict.fromkeys(wordlist)
        makeTrans = lingo.woordenDB.makeTrans()
        for w in lingo.woordenDB.generaliseer(wordlist, self.checkInterrupt):
            # find word in list
            canonical = w.lower().translate(
                makeTrans,
                lingo.woordenDB.metTekens).replace('ij', 'Y')
            if result[canonical] is None: result[canonical] = []
            result[canonical].append(w)
        return result

    def isAllowed(self, word):
        """Check if a word is allowed with the current settings.
        """
        if not lingo.woordenDB.metAccenten and max(word) > 0x80:
            return False
        if not lingo.woordenDB.metHoofdletters and set(letterList) & set(word):
            return False
        if set("-.' ") - set(lingo.woordenDB.metTekens) & set(word):
            return False
        return True

    def generalise(self, word):
        """Write a canonical word back into a word without restrictions
        (allowing capitals, accents and extra characters).
        If interrupted, or if the word does not need any of this,
        None is returned.
        """
        expandedWord = word.replace('Y', 'ij')
        result = None
        try:
            # check all words that could be the same
            for w in lingo.woordenDB.generaliseer([word], self.checkInterrupt):
                if w == expandedWord:
                    # the identical word occurs: nothing to report
                    return None
                if result is None:
                    # we have a variant: keep if new, continue
                    result = w
            return result
        except Interrupt:
            return None

    def stop(self, arg):
        self.keepRunning = False

    def restart(self, arg):
        """Word size changed: start reading all words
        interrupted if a different operation is needed
        """
        size = int(arg)
        Logger.debug("{:5.2f} Guesser.restart({})".format(clock() % 60, size))
        self.status = "Inlezen", BUSY
        self.clearThoughts()
        try:
            self.setThoughts(0, 'Inlezen woorden van {:d} letters'.format(size))
            self.allA = self.readWords("." * size)
            self.setThoughts(0, "Er zijn {} woorden van {} letters".format(
                *self.allA.shape))
            randomWord = woordenA2strings(self.rader.woordenA[
                randrange(len(self.rader.woordenA))])[0]
            self.setThoughts(1, "Suggestie: " + randomWord)
            # later, update suggestion with the proper spelling
            try:
                generalised = next(lingo.woordenDB.generaliseer([randomWord]))
                self.setThoughts(1, "Suggestie: " + generalised)
            except StopIteration:
                pass
        except Interrupt:
            self.allA = self.rader = None
            self.setThoughts(0, "")

    def forget(self, arg):
        """remove word temporarily, so another word can be guessed
        See WoordRader.keurGokAf
        The word is removed from rader.woordenA and patternA,
        but not from dictionary (rader.mogelijkA is recomputed by guess)
        """
        # not needed if there is no rader
        if self.rader is None:
            Logger.info("{:5.2f} Guesser.forget: nothing to do".format(
                clock() % 60))
            return
        # take last word from word that still is in previous reply
        lastWord = self.reply[0]
        Logger.info("{:5.2f} Guesser.forget {}".format(
            clock() % 60,
            lastWord))
        wordA = string2woordenA(lastWord)
        # remove from the current arrays and adjust thoughts
        self.rader.woordenA = self.rader.woordenA[
            (self.rader.woordenA != wordA).any(axis=1)]
        woordenShape = self.rader.woordenA.shape
        if max(self.rader.patroon) == '.':
            self.setThoughts(0, "Er zijn nog {} woord{} van {} letters".format(
                woordenShape[0],
                ('en' if woordenShape[0] != 1 else ''),
                woordenShape[1]))
        self.patternA = self.patternA[
            (self.patternA!=wordA).any(axis=1)]
        self.setThoughts(1, "{} woord{} voldoen aan {}".format(
            len(self.patternA),
            ('en' if len(self.patternA) != 1 else ''),
            self.pattern))

    def correct(self, arg):
        """Rebuild corrections to the dictionaries,
        so that unnecessary words are removed.
        """
        self.status = "Bijwerken", BUSY
        # get all words in correcties, with flag if they occur
        exists = lingo.woordenDB.relevanteWoorden(())
        correcties = lingo.woordenDB.correcties
        for w,f in exists.items():
            if correcties[w] == '-+'[f]:
                # no need to keep in correcties
                del correcties[w]
        correcties.sync()
        try: self.setStatusWoorden(self.patternA)
        except AttributeError: self.status = "Klaar", HAPPY

    def update(self, arg=None):
        """adjust rader to changed settings
        """
        Logger.info("{:5.2f} updating settings".format(
            clock() % 60))
        config = self.config
        self.setMargins(config)
        # copy app settings to lingo module
        lingo.MAXTHINKTIME = config.getfloat("spel", "denktijd")
        lingo.PRECISIE = config.getint("intern", "sample")
        lingo.AANTALBESTE = config.getint("intern", "attempts")
        lingo.INFORMATIEMARGE = config.getfloat("intern", "loss")
        self.updateDictionaries(config)

    def setMargins(self, config):
        """Configure guessing behaviour
        posMargin: number of bits lost before choosing weird word
        allMargin: extra number of bits lost for all
        """
        self.posMargin, self.allMargin = {
            "nooit":  (NEVER, NEVER),
            "soms":   (1., .3),
            "vaak":   (.5, .2),
            "altijd": (.3, .05),
            }[config.get("spel", "raden")]
        if config.getint("spel", "gegeven"):
            self.allMargin = NEVER

    def updateDictionaries(self, config):
        """Check if dictionary settings changed,
        and reconstruct lingo.woordenDB if needed
        """
        folder = config.get("dictionaries", "folder")
        files = json.loads(config.get("dictionaries", "files"))
        exceptions = config.get("dictionaries", "exceptions")
        if ((self.folder, self.files, self.exceptions)
                != (folder, files, exceptions)):
            Logger.info("{:5.2f} updating woordenDB".format(
                clock() % 60))
            self.folder, self.files, self.exceptions = folder, files, exceptions
            # remove rader to force reload
            Logger.debug("{:5.2f} Clearing rader".format(clock () % 60))
            lingo.woordenDB = lingo.WoordenDB(
                [os.path.join(lingo.SCRIPTDIR, folder, f) for f in files],
                os.path.join(lingo.SCRIPTDIR, folder, exceptions))

        toestaan = config.get("spel", "toestaan")
        Logger.debug("{:5.2f} extra features: {}".format(clock() % 60, toestaan))
        lingo.woordenDB.metAccenten = "accenten" in toestaan
        lingo.woordenDB.metHoofdletters = "hoofdletters" in toestaan
        metTekens = ''
        if "streepjes" in toestaan: metTekens += '-'
        if "puntjes" in toestaan: metTekens += '.'
        if "apostrofs" in toestaan: metTekens += "'"
        if "spaties" in toestaan: metTekens += ' '
        lingo.woordenDB.metTekens = metTekens

        self.rader = None

    def dump(self, arg):
        Logger.debug("{:5.2f} debug dump {}".format(
            clock() % 60,
            self))
        Logger.debug("Thoughts: {}".format(self.thoughts.replace('\n',';')))
        Logger.debug("self.pattern: {}".format(self.pattern))
        possibleA, patternA, allA = self.computeSets()
        Logger.debug("possibleA={}: {}".format(
            len(possibleA),
            ','.join(sorted(woordenA2strings(possibleA))[:1000])))
        Logger.debug("patternA={}: {}".format(
            len(patternA),
            ','.join(sorted(woordenA2strings(patternA))[:1000])))
        Logger.debug("allA={}: {}".format(
            len(allA),
            ','.join(sorted(woordenA2strings(allA))[:1000])))
            
    # auxiliary guessing routines
    def dataline2wordGuess(self, row):
        """Given a row number from data,
        determine the guess and piepjes
        for verwerkGok van lingo
        Raises a TypeError is there is a grey or white cell
        """
        line = self.data[row]
        guess = ''.join(line['letter'])
        uitkomst = ''.join(map(
            {BLUE:'.', RED:'*', YELLOW:'+'}.get,
            line['color']
            ))
        return guess, uitkomst

    def computeSets(self):
        """Compute the three sets with:
        words that could be a solution
        words that match the initial pattern
        and all words of that length
        as a side effect, self.rader.mogelijkA = all possible solutions
        """
        self.status = "Zoeken", BUSY
        pattern, cheatFlag = self.recheckPattern()
        #Logger.debug("{:5.2f} GuessGrid.computeSets pattern {}".format(clock() % 60, pattern))
        # getPatternA adjusts thoughts[1] automatically
        patternA = self.getPatternA(pattern)
        # subtly show that you saw what the user did
        if cheatFlag: self._thoughts[1] += ' (?)'
        # getPatternA adjusts thoughts[2:4] automatically
        possibleA = self.getPossible(patternA)
        #Logger.debug("{:5.2f} GuessGrid.computeSets status {}".format(clock() % 60, status))
        self.setStatusWoorden(possibleA)
        return possibleA, patternA, self.rader.woordenA

    def summarize(self, wordsA):
        """adapted from lingo:Mens.matchPattern
        """
        # letters that definitely are in there
        certainLetters = set(range(len(letterList)))
        for word in wordsA:
            certainLetters.intersection_update(word)

        # determine possible letters per position
        letterSets = map(set, wordsA.T)

        # layout
        result = []
        for letterSet in letterSets:
            # first the letters that are certainly correct
            possibleLetters = letterSet.intersection(certainLetters)
            formatted = b''.join(index2letter[list(possibleLetters)])
            # now the others
            rest = letterSet.difference(certainLetters)
            if len(rest)==0: pass
            elif len(rest)<=2:
                formatted += b','+b''.join(index2letter[list(rest)])
            elif len(rest)<10: formatted += str(len(rest)).encode()
            else: formatted += b'*'
            result.append(formatted.decode())
        return ' '.join(result)

    def getPossible(self, patternA):
        """find all possible words that match the guesses
        pass patternA as argument that you have to compute with:
            self.getPatternA(self.recheckPattern()[0])
        adjusts thoughts[2:4]
        """
        #Logger.debug("{:5.2f} Guesser.getPossible({})".format(clock() % 60, len(patternA)))
        rader = self.rader
        # set rader.mogelijkA to all words matching pattern of first line
        self.rader.mogelijkA = patternA
        # process the lines; stop if we apparently are ready
        for i,line in enumerate(self.data):
            if (line['color'] == RED).all():
                raise Interrupt("Al klaar")
            if (line['color'] < BLUE).any():
                if i < len(self.data) - 1:
                    # stop guessing if there is a grey/white cell
                    # but lower line is allowed
                    raise Interrupt("Onduidelijk")
                break
            guess, result = self.dataline2wordGuess(i)
            self.rader.verwerkGok(string2woordenA(guess), result)
        possibleA = self.rader.mogelijkA
        self.setThoughts(2, "{} woord{} mogelijk:".format(
            len(possibleA),
            (" is" if len(possibleA) == 1 else "en zijn")))
        if len(possibleA) * (1 + possibleA.shape[1]) < LINELENGTH:
            self.possibleWords = sorted(woordenA2strings(possibleA))
            self.setThoughts(3, ','.join(self.possibleWords))
        else:
            summary = self.summarize(possibleA)
            if len(summary) < LINELENGTH:
                self.setThoughts(3, self.summarize(possibleA))
        return possibleA

    def recheckPattern(self):
        """Give the initial information for the first guess.
        If somebody changed the given letters,
        (basically, breaking the rules!)
        create a new pattern from that, and return True as second result
        """
        firstWord, firstLinePiepjes = self.dataline2wordGuess(0)
        firstPattern = ''.join((w if p == '*' else '.')
                            for p,w in zip(firstLinePiepjes, firstWord))
        if self.moreRestrictive(firstPattern, self.pattern):
            # no problem here
            return self.pattern, False
        else:
            # ouch, somebody changed a given letter?!
            Logger.warning("{:5.2f} Player is cheating with first reply {}".
                format(clock() % 60, firstLinePiepjes))
            self.status = "Opnieuw", BUSY
            return firstPattern, True

    @staticmethod
    def moreRestrictive(pattern1, pattern2):
        """Check if words matching pattern1 also match pattern2
        """
        return len(pattern1) == len(pattern2) \
            and all(c1 == c2 or c2 == '.'
                for c1,c2 in zip(pattern1, pattern2))

    # guessing words
    def initialGuess(self, pattern):
        """Guess the first word,
        with zero or more given letters.
        sets self.reply
        """
        Logger.info("{:5.2f} Guesser.initialGuess({})".format(
            clock() % 60,
            pattern
            ))
        self.status = "Raden", BUSY
        self.pattern = pattern
        # initialize rader.mogelijkA before calling kiesRaadWoord
        possibleA = self.getPatternA(pattern, cache=False)
        self.rader.mogelijkA = self.patternA = possibleA
        lenPossible = len(possibleA)
        self.setThoughts(1, "{} woord{} aan {}".format(
            lenPossible,
            ('en voldoen' if lenPossible != 1 else ' voldoet'),
            pattern))
        self.setStatusWoorden(possibleA)
        # impossible, or unique, or no reason to take a set
        if len(possibleA) < 3:
            self.guessSimpleCase(possibleA)
            return
        # do the computation
        gokA, Q, stopVlag = self.rader.kiesRaadWoord(possibleA)
        guess = woordenA2strings(gokA)[0]
        self.setThoughts(4, "beste woord met Q={:4.2f}: {}".format(
            Q, guess))
        Logger.info("{:5.2f} kiesRaadWoord eerste gok: {} Q={:.1f}".format(
                     clock() % 60,
                     guess,
                     Q))
        self.reply = guess, False

    def guess(self):
        """calls chooseGuess one to three times, with:
            possible words: if more than one line
            words matching pattern: if one line or possibleA not good
            all words: if available and patternA still not good
        Returns guess word and a flag if the word is the only choice
        sets self.reply
        """
        Logger.info("{:5.2f} Guesser.guess {}->{}".format(
            clock() % 60,
            ','.join(map(''.join, self.data['letter'])),
            (self.dataline2wordGuess(-1)[1]
             if (self.data[-1]['color'] >= 0).all()
             else '?'),
            ))
        self.status = 'Raden', BUSY
        self.clearThoughts(1)
        possibleA, patternA, allA = self.computeSets()
        self.setStatusWoorden(possibleA)
        # impossible, or unique, or no reason to take a set
        if len(possibleA) < 3:
            self.guessSimpleCase(possibleA)
            return
        # do the computation
        self.reply = self.bestWord(possibleA, patternA, allA), False

    def guessSimpleCase(self, possibleA):
        """Handle the cases where there are fewer than 3 possible words.
        Used by initialGuess and guess
        Returns guess word and a flag if the word is the only choice
        sets self.reply
        """
        lenPossible = len(possibleA)
        if lenPossible > 3:
            raise ValueError("guessSimpleCase: too many options {}".format(
                lenPossible))
        if lenPossible:
            self.setThoughts(4, "Ik weet maar {} woord{}; ik gok".format(
                lenPossible,
                ('en' if lenPossible != 1 else '')))
            self.reply = woordenA2strings(possibleA)[0], (lenPossible == 1)
        else:
            self.setThoughts(4, "Ik weet geen oplossing")
            self.reply = '?' * possibleA.shape[1], False
            alternatives = list(islice(self.findGenericSolution(), 5))
            if alternatives:
                self.setThoughts(5, "Bedoel je misschien (niet toegestaan):")
                self.setThoughts(6, ', '.join(alternatives))

    def getPatternA(self, pattern, cache=True):
        """get words matching given pattern
        optimized for cases where pattern hasn't changed
        or pattern is a subset of words that are read
        adjusts thoughts[1]
        May produce Interrupt if the words have to be reread
        """
        # select the fastest way to get a result
        if cache and self.pattern == pattern and self.rader:
            Logger.debug( "{:5.2f} getPatternA reusing pattern {}".format(clock() % 60, pattern))
            resultA = self.patternA
        elif self.rader and self.moreRestrictive(pattern, self.rader.patroon):
            # select words from rader.woordenA to match pattern
            Logger.debug("{:5.2f} getPatternA pattern {}->{}".format(
                clock() % 60,
                self.rader.patroon,
                pattern,
                ))
            self.setThoughts(1,
                "Zoeken woorden die voldoen aan {}".format(pattern))
            resultA = self.rader.woordenA
            for i,c in enumerate(pattern):
                if c=='.': continue
                resultA = resultA[resultA[:,i] == letter2index[ord(c)]]
        else:
            # we'll have to start over. Reread the words
            Logger.debug("{:5.2f} getPatternA: start over".format(clock() % 60))
            self.setThoughts(1,
                "Inlezen woorden die voldoen aan {}".format(pattern))
            resultA = self.readWords(pattern)
        self.setThoughts(1,
            "{} woorden voldoen aan {}".format(len(resultA), pattern))
        return resultA

    def bestWord(self, possibleA, patternA, allA):
        """Try three different ways to find the best word.
        This is where most of the computation goes
        adjusts thoughts[4:7]
        """
        # try a possible word
        gokA, Q1, stopVlag = self.rader.kiesRaadWoord(possibleA)
        gok1 = woordenA2strings(gokA)[0]
        self.setThoughts(4, 'Mogelijke woord "{}" geeft Q={:.2f}'.format(
            gok1,
            Q1))
        if Q1 == 0.0:
            self.setThoughts(6, "Dit is optimaal")
            return gok1
        if stopVlag:
            self.setThoughts(6, "Ik kan net zo goed gokken")
            return gok1

        # try a pattern word (if there are more pattern than possible words)
        if len(patternA) == len(possibleA):
            Q2, gok2 == Q1, gok1
        else:
            gokA, Q2, _ = self.rader.kiesRaadWoord(patternA)
            gok2 = woordenA2strings(gokA)[0]
            self.setThoughts(5, 'Passende woord "{}" geeft Q={:.2f}'.format(
                gok2,
                Q2))

        if Q2 == 0.0:
            self.setThoughts(6, "Dit is optimaal")
            return gok2

        # try all words, if allowed and useful
        if self.allMargin == NEVER or len(allA) == len(patternA):
            Q3, gok3 = Q2, gok2
        else:
            gokA, Q3, _ = self.rader.kiesRaadWoord(allA)
            gok3 = woordenA2strings(gokA)[0]
            self.setThoughts(6, '"{}" geeft Q={:.2f}'.format(
                gok3,
                Q3))

        # write some statistics for posterity
        if Q1 > Q2 > Q3:
            open("stats.csv", "a").write('"' + self.pattern +
                '",{},{:.2},{},{:.2},{},{:.2}\n'.format(
                len(possibleA), Q1,
                len(patternA), Q2,
                len(allA), Q3,
                ).replace(',',';').replace('.',','))

        # select the proper word depending on settings
        if Q1 - Q3 < self.posMargin:
            self.setThoughts(7, "Ik raad maar een mogelijk woord")
            return gok1
        if Q2 - Q3 < self.allMargin and len(allA) != len(patternA):
            self.setThoughts(7, "Ik raad maar een passend woord")
            return gok2
        self.setThoughts(7, "Ik ga voor maximale informatie")
        return gok3

    def readWords(self, pattern):
        """Read words from the official dictionaries (with corrections).
        Sets self.rader for this pattern
        returns all words matching pattern or None
        Will be interrupted by any command,
        except, if the pattern is only dots, a restart command
        with exactly that width
        Shows results to user in status but not in thoughts
        """
        #Logger.debug("{:5.2f} Guesser.readWords({})".format(clock()%60, pattern))
        self.status = 'Inlezen', BUSY
        # may be interrupted
        if self.rader is None or self.rader.patroon != pattern:
            self.pattern = pattern
            self.rader = lingo.WoordRader(pattern, self.checkInterruptWithIgnore)
        self.setStatusWoorden(self.rader.woordenA)
        return self.rader.woordenA

    def checkInterruptWithIgnore(self):
        self.checkInterrupt(True)

    def checkInterrupt(self, allowIgnore=False):
        """raises Interrupt if restart process needs to be interrupted:
        basically always, unless the same width is requested again
        used by readWords
        """
        # allow foreground processes if they feel like it
        sleep(1e-3)
        if not self.changed.isSet():
            # no message, nothing to worry about
            return
        if allowIgnore:
            # for readWords: ignore interrupt by restart if that makes no difference
            message = self._message
            rader = self.rader
            if (
                    isinstance(message, str) and 
                    message.startswith('restart') and
                    int(message[7:]) == len(self.pattern)):
                # restart: ignore when is has the same length
                Logger.debug("{:5.2f} ignoring interrupt()".format(clock() % 60))
                self.changed.clear()
                return
        # all other cases
        raise Interrupt("Onderbroken")

    # if the word couldn't be found
    def checkResults(self, word):
        """Generate locations where users inputs do not
        correspond to the word
        """
        self.clearThoughts(5)
        self.setThoughts(6, "Ingevoerd: " + word)
        errors = 0
        woordA = string2woordenA(word).reshape(1, -1)
        guessesA = strings2woordenA(self.data['letter'])
        for row, guessA in enumerate(guessesA[:-1]):
            actual = self.rader.maakPiepjes(woordA, guessA)
            guess, claimed = self.dataline2wordGuess(row)
            Logger.debug("{:5.2f} [{}]: {}->{}".format(
                clock() % 60,
                row,
                actual,
                claimed))
            for col in self.compareGuess(guess, actual, claimed):
                errors += 1
                self.setThoughts(7, "Sorry, {} fout{}".format(
                    errors,
                    ('' if errors == 1 else "en")
                    ))
                yield row, col
        Logger.info("{:5.2f} checking user entry. errors: {}".format(
                clock() % 60,
                errors))

    def compareGuess(self, word, good, entered):
        """Compare the piepjes from "good" and "entered".
        The "entered" piepjes are allowed to be non-standard
        in the sense that + and . may be permuted for the same letter
        (almost never occurs...)
        """
        # per letter what is expected, and list of error positions
        expectedPerChar = {}
        for pos, (char, goodP, enterP) in enumerate(zip(word, good, entered)):
            if goodP == enterP:
                continue
            if goodP == '*' or enterP == '*':
                yield pos
                continue
            # goodP, enterP are +,. in some order
            if char not in expectedPerChar:
                expectedPerChar[char] = [goodP]
            expected = expectedPerChar[char]
            if expected[0] == goodP:
                # another (or first) error in same direction
                expected.append(pos)
            else:
                # compensation in other direction
                expected.pop()
                if len(expected) == 1:
                    # clean up
                    del expectedPerChar[char]
        for posL in expectedPerChar.values():
            for pos in posL[1:]:
                yield pos

    def makeGenericSearchPattern(self):
        """Make a pattern consisting of al known letters and dots elsewhere
        """
        knownLetters = numpy.where(
            self.data['color'] == RED,
            self.data['letter'],
            '.')
        # take column maximum, but we have to switch to int8 temporarily
        knownLetters = knownLetters.view('int8').max(axis=0).view('S1')   
        return ''.join(knownLetters)

    def findGenericSolution(self):
        """Find all words that are probable solutions
        given the current guesses.
        Returns Latin1 version of the original words,
        with any kind of "extra" such as capitals, accents or extra characters
        interrupted by any command.
        Will automatically use the guesses that have no grey or dark red
        colored cells.
        """
        # find how many lines have useful data (no grey or dark red)
        badLines = (self.data['color'] < BLUE).any(1)
        if badLines.all():
            # no values at all; producing the entire dictionary is useless
            return
        # argmax gives the location of the first True,
        # but if all are False, gives 0. In that case, use all lines.
        guessesA = strings2woordenA(self.data['letter'][:
            badLines.argmax() or None])
        replaceTable = lingo.woordenDB.makeTrans()
        # find words that match in general with the red letters
        pat = self.makeGenericSearchPattern()
        try:
            for word in lingo.woordenDB.generaliseer([pat], self.checkInterrupt):
                # handle words that differ in ij treatment
                # check if word matches the guesses
                # make canonical including extra characters
                wordT = word.translate(replaceTable, "-.' ").lower().replace("ij","Y")
                if  len(wordT) != len(pat): continue
                woordA = string2woordenA(wordT).reshape(1, -1)
                for row, guessA in enumerate(guessesA):
                    actual = self.rader.maakPiepjes(woordA, guessA)
                    guess, claimed = self.dataline2wordGuess(row)
                    try:
                        next(self.compareGuess(guess, actual, claimed))
                    except StopIteration:
                        # this may be the word, we have a match, try next line
                        continue
                    # this isn't the word, we found an error
                    break
                else:
                    # this could be the word, all lines work
                    yield word
        except Interrupt:
            pass
