"""grid.py
Defines GuessGrid, that determines how to show the words.
"""

from numpy import array, dtype, int8, newaxis, place
from numpy import string_, vstack, where, zeros, ones
from collections import OrderedDict
import lingo
from lingo import letter2index

DARKRED, GREY, BLUE, RED, YELLOW, WHITE = xrange(-2, 4)
coloredLetter = dtype([('letter', string_, 1), ('color', int8)])

from kivy.logger import Logger
from time import time as clock

class DisplayStatus(object):
    """DisplayStatus describes what is supposed to be on screen
    Behaves as 2d array of words and letters,
    where each letter has a background color
    currentLine is the line number where to fill in the next word;
    need not exist
    coding of colors:
    color piep mode meaning
    darkred    -2   unknown, but can be concluded from dictionary
    grey       -1   unknown
    blue   .    0   not in word
    red    *    1   correct letter at correct place
    yellow +    2   correct letter, different place
    white       3   letter to set initially
    """
    def __init__(self, width):
        self.data = zeros((0, width), coloredLetter)
        self.data['color'] = GREY
        self.currentLine = 0

    def dataFormatter(self, data):
        """For testing: show the grid in a nice way.
        """
        return '; '.join(
            '{}:{}'.format(''.join(line['letter']),
                           ''.join(map(str, line['color'])))
            for line in data)

    def putWord(self, word, colors=None):
        """Put given word on the current line.
        Increases height if needed;
        return True in that case
        """
        Logger.debug("{:5.2f} DisplayStatus.putWord({}, {}, {})".format(
                     clock() % 60,
                     self.currentLine,
                     word,
                     colors))
        if colors is None: colors = [GREY]*len(word)
        a = array(zip(word, colors), coloredLetter)
        if self.currentLine < len(self.data):
            self.data[self.currentLine] = a
            return False
        else:
            Logger.debug("{:5.2f} new line [{}] = {}".format(
                         clock() % 60,
                         self.currentLine,
                         word))
            self.data = vstack((self.data, a))
            return True

    # TODO: remove? old interface
    def addWords(self, word, colors=None):
        """Add or more words to the grid
        """
        if colors is None: colors = [GREY]*len(word)
        a = array(zip(word, colors), coloredLetter)
        a.shape = -1, self.data.shape[1]
        self.data = vstack((self.data, a))


class GuessGrid(DisplayStatus):
    """A DisplayStatus with additional functionality for screen update
    via buttonGrid
    """
    def __init__(self, buttonGrid):
        self.buttonGrid = buttonGrid
        # this call implicitly does the DisplayStatus.__init__
        self.setWidth(buttonGrid.cols)

    def __repr__(self):
        if hasattr(self, "data"):
            data = ','.join(map(''.join, self.data["letter"]))
        else:
            data = ""
        return "{}({})".format(
                self.__class__.__name__,
                data)

    def putWord(self, word, sure=False):
        """Put word at the current line with the proper colors.
        """
        if '?' in word: word = self.makeUnknown()
        Logger.info("{:5.2f} GuessGrid.putWord: [{}]={}".format(
                    clock() % 60,
                    self.currentLine,
                    word))
        if super(GuessGrid, self).putWord(word, self.colors(word, sure)):
            self.buttonGrid.addLetters(word)
        for col,lc in enumerate(self.data[self.currentLine]):
            self.changeLetter(
                self.currentLine,
                col,
                lc['letter'],
                int(lc['color']))

    def colors(self, word, sure=False):
        """Determine the colors of new word
        Red of letter is known good, grey otherwise
        """
        # bepaal per kolom of er een letter rood was
        redword = array(zip(word, [RED]*len(word)), coloredLetter)
        result = where(self.data==redword[newaxis,:], RED, GREY).max(axis=0)
        if sure:
            result[result == GREY] = DARKRED
        return result

    def makeUnknown(self):
        """Make a word with question marks,
        but with sure letters filled in.
        """
        # conveniently, ? is smaller than all letters
        result = ''.join(map(
            max,
            where(self.data['color'] == RED,
                  self.data['letter'],
                  '?').T
            ))
        if '?' not in result:
            result = '?' * self.data.shape[1]
        return result

    def setMode(self, row, col, mode):
        """Change color of a single cell
        """
        Logger.info("{:5.2f} GuessGrid.setMode[{},{}]={}->{}".format(
                    clock() % 60,
                    row, col, self.data[row,col]['letter'], mode))
        self.data[row, col]['color'] = mode
        # teken het effect op het scherm
        self.buttonGrid.setMode(row, col, mode)

    # de routines hieronder praten met buttonGrid
    def setWidth(self, width):
        """Change width, and set screen to row of dots.
        """
        # set the screen to one line with a white cell
        self.buttonGrid.setWidth(width)
        # reset DisplayStatus
        super(GuessGrid, self).__init__(width)
        # initialize my array to match screen
        self.addWords('.' * width)

    def changeLetter(self, row, col, text, color=None):
        """Zet een letter goed, in data en op het scherm.
        """
        cel = self.data[row, col]
        #Logger.debug("{:5.2f} GuessGrid.changeLetter[{},{}]->{}".format(clock() % 60, row, col, text))
        cel['letter'] = text
        if color is None:
            color = int(cel['color'])
        else:
            cel['color'] = color
        self.buttonGrid.setMode(row, col, color, text)

    def clickGrid(self, row, col):
        """User pressed a letter.
        Adjust color. (Only this one for now)
        """
        Logger.debug("{:5.2f} GuessGrid.clickGrid {},{}".format(
            clock() % 60, row, col))
        # ignore question marks, don't use new row
        if self.data[row, col]['letter'] == '?':
            Logger.error("{:5.2f} GuessGrid.clickGrid: question mark!".format(
                clock() % 60
                ))
        else:
            mode = int(self.data['color'][row, col] + 1) % 3
            self.setMode(row, col, mode)

    def readyToGuess(self, row=None):
        """Check if the grid is ready for guessing.
        Uses the information provided by calling clickGrid.
        This means that this routine doesn't work in the intial stage
        when there are only EntryButtons.
        True if:
            multiline grid: no grey cells
            row < rows of given
        """
        colors = self.data['color']
        letters = self.data['letter']
        if (letters == '.').any():
            # guess with dots if nothing is white anymore
            return (colors != WHITE).all()
        elif (colors[-1] == RED).all():
            # stop if correct word
            return False
        elif row is not None and row < len(letters) - 1:
            # guess again if older input changed
            # even if there is grey
            return True
        elif colors.min() < BLUE:
            # don't guess if anything grey (or dark red)
            return False
        return True

    def unknownWord(self):
        return (self.data[-1]['letter'] == '?').all()

    def redLine(self):
        return (self.data[-1]['color'] == RED).all()
