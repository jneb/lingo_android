#!/usr/bin/env python2
"""Speel Lingo.
Argument: het woord dat ik ga raden; anders moet je zelf de "piepjes" intikken
in de vorm *..++. of zo.
"""

#algoritme:
#Probeer van alle woorden wat de mogelijke antwoorden zijn met de woorden
#die nu nog relevant zijn
#kies het woord dat de meeste Shannon informatie geeft.
#met andere woorden: de som van n log n van de aantallen woorden die bij de
#verschillende piepjespatronen optreden moet minimaal zijn.
#TODO: engels?

from __future__ import division, print_function
import os, sys, re, time
if sys.version_info.major == 2:
    input = raw_input
    range = xrange
    from string import maketrans
else:
    maketrans = str.maketrans
from string import ascii_lowercase
from random import uniform, randrange, sample
from heapq import nsmallest
from bisect import bisect_right
from functools import partial

from numpy import arange, argsort, array, bincount, concatenate, errstate
from numpy import fromiter, fromstring, int8, log2, maximum, minimum, newaxis
from numpy import ptp, resize, searchsorted, string_, unique, where, zeros

from numpy.random import random_sample, shuffle
from operator import concat, attrgetter, itemgetter, methodcaller
from collections import Counter, defaultdict


"""
Rekenwerk voor dit programma.
    Om een goed woord te kiezen, definieer ik de "Shannon-waarde" van een woord.
    Als je het gekozen woord als een stochast ziet, kan de informatie die een
    geraden woord over deze stochast geeft, berekenen.
    Als je een woord raadt, kan je verschillende patronen "piepjes" krijgen
    neem c het aantal woorden dat met een patroon overeenkomt
    n het totale aantal mogelijke woorden (dus som(c))
    dan is de informatie van een gok (we nemen de 2log voor bits):
    -som(c/n * log(c/n)) =
    = -1/n * som(c * (log(c) - log(n))) =
    = -1/n * ( som(c*log(c)) - n*log(n) )
    = som(c*log(c)) * A + B
    met A = -1/n en B = log(n) onafhankelijk van de keuze

    Die som(c*log(c)) noem ik "kwaliteit"
    de beste woorden geven de laagste kwaliteit
    omdat we log2 nemen, krijg je bits als eenheid,
    dus een afwijking van d naar boven is d bits verlies in het raden,
    dit geeft aan hoe ver een woord van optimaal is.
    INFORMATIEMARGE is de hoeveelheid bits die we bereid zijn te verliezen
    om het raden interessant te maken

    Het is natuurlijk slim om woorden te kiezen met een lage kwaliteit
    maar het is nogal duur om uit te rekenen, en saai om altijd dezelfde te zien

    Daarom berekenen we het zo:
    1- meet de kwaliteit met een steekproef uit toegestane woorden
       (grootte van de steekproef heet PRECISIE)
    -  neem de woorden uit bron A met goedeVolgorde om een beetje slim te doen
    -  stop met rekenen als MAXTHINKTIME verstreken is
    -  kies de beste AANTALBESTE eruit
    2- meet van deze woorden de kwaliteit precies, en sorteer ze
    3- kies een woord dat INFORMATIEMARGE van het maximum af ligt

    Op grond van metingen vermoed ik:
    - het aantal mogelijke woorden kan oplopen tot een paar duizend
      bijvoorbeeld: ...e..... geeft 5195 en .e...... geeft 6208
    - als je alle woorden van een lengte toestaat, wordt het max. 37035
    - PRECISIE mag relatief laag zijn, bij 200 krijg ik nog goede resultaten
      als het aantal woorden rond de 5000 zit
    - AANTALBESTE moet groot genoeg zijn om de goede woorden te vinden
      (twee keer het aantal gewenste lijkt goed)
    - goedeVolgorde zorgt dat de goede woorden meestal in de eerste 1/4
      van de woorden zit

    Schatting rekentijd stap 2:
    We gaan even uit van AANTALBESTE=50 en PRECISIE=200
    - als lb het aantal woorden in bronA is dat is gedaan in stap 1
      (we hopen dat dit flink groter is dan AANTALBESTE)
    - lm het aantal woorden in mogelijkA,
    - t de tijd van stap 1
    stap 1 bestaat uit PRECISIE*lm berekeningen, en dat willen we ook
    ongeveer voor stap 2.
    Omdat we beslist AANTALBESTE woorden willen schatten, wordt de precisie
    automatisch gelijk aan PRECISIE*lm/AANTALBESTE

    Bij het eindspel komt nog iets interessants: soms is er een groot aantal
    woorden die het kan zijn, maar levert het raden weinig op.
    Dan is het zinnig om te overwegen een woord te raden dat zeker fout is.
    Typisch geval: je hebt n woorden met **.**** of iets dergelijks
    In dat geval, als er maximaal 1 2 in de groepgroottes zit
    (of de som(qlogq) is kleiner dan 2, als je het zo wil zien)
    kan je beter een mogelijk woord raden,
    anders is de kans om direct raak te raken relatief klein,
    dus dan raad je gewoon een woord wat veel informatie biedt.

    de hoeveelheid informatie die verloren mag gaan bij het optimaliseren
    (in bits) hoger maakt de raadwoorden "dom", lager maakt ze "saai"
    advies: .2
"""

INFORMATIEMARGE = .3

#default lengte van een woord
DEFAULTLENGTE = 6
#maximum tijd die computer mag nadenken, in seconden.
#Kan twee keer zo lang worden
MAXTHINKTIME = .1

#aantal woorden om mee te nemen bij het bepalen van de informatiewaarde
#hoger geeft dat hij minder woorden uitprobeert om een goede te vinden
#lager is minder nauwkeurig
#advies: sqrt(MAXTHINKTIME*2e5), waarbij de 2e5 evenredig is met je processor
PRECISIE = 200


#maximaal aantal woorden die goed zijn om uit te kiezen
#hoger brengt soms iets meer variate in de raadwoorden
AANTALBESTE = 50

# size of read chunks
BLOCKSIZE = 100000

SCRIPTDIR = os.path.dirname(
    sys.executable if hasattr(sys, 'frozen') else __file__)
WOORDENLIJSTEN = [
    os.path.join(SCRIPTDIR, 'woordenlijst.utf8'),
    os.path.join(SCRIPTDIR, 'woordenlijst2.utf8'),
    ]
WOORDENLIJSTEN = [
    os.path.join(SCRIPTDIR, 'wordlists', 'OpenTaal-220.txt'),
    ]
CORRECTIES = os.path.join(SCRIPTDIR, 'corr-woordenlijst.shelve')
CORRECTIES = os.path.join(SCRIPTDIR, 'wordlists', 'exceptions.shelve')

class SimpleShelve(dict):
    "A DB in the form of a shelved dictionary"
    #make sure pickle is available for me at close time, to prevent errors
    import pickle

    def __init__(self, filename, mode='c'):
        """Flags:
        r: read only
        w: r/w
        c: create if needed
        n: clear before use
        """
        super(SimpleShelve,self).__init__()
        self.filename = filename
        self.mode = mode
        self.closed = False
        assert mode in list('rwcn')
        if mode!='n':
            try: self.update(self.pickle.load(open(self.filename,'rb')))
            except IOError:
                if mode=='c': pass
                else: raise

    # make sure open is available when needed
    def sync(self, open=open):
        if self.closed or self.mode=='r': return
        self.pickle.dump(dict(self), open(self.filename,'wb') ,2)

    def close(self):
        self.sync()
        self.closed = True

    def __del__(self): self.close()


#Terminologie:
#string: een woord in een string. Y staat voor de letter ij
#strings: een array van strings
#woordA: een woord als een array, 1 dimensionaal. Letters zijn indices
#woordenA: een aantal woorden als een array, 2 dimensionaal, woord*kolom
#gokS: een woord wat wordt geraden, als een string
#gokA: een woord wat wordt geraden, als een woordA
#piepjesS: een string met *, + en .
#patroon: een string met . en een paar letters
#sterA: een array van meerdere woorden, 1 waar een letter overeenkomt
#plusA: een array van meerdere woorden, met aantal keren dat letter voorkomt
#  (het aantal keren dat de letter op de plaats uit gok in het woord voorkomt,
#   gemaximaliseerd op het aantal keren dat deze letter in gok voorkomt)

#behandel ij als een letter
letterlijst = 'Y'+ascii_lowercase
index2letter = fromstring(letterlijst, (string_, 1))
letter2index = zeros(128, int8)-1
letter2index[index2letter.view(int8)] = arange(27, dtype=int8)

def woordenA2strings(a):
    """Converteer woordA of woordenA a naar een array van strings.
    >>> a = letter2index[fromstring('testprYs',int8).reshape((2,4))]
    >>> woordenA2strings(a)
    array(['test', 'prYs'],
          dtype='|S4')
    >>> woordenA2strings(a[0])
    array(['test'],
          dtype='|S4')
    """
    return index2letter[a].view((string_, a.shape[-1])).reshape(-1)

def strings2woordenA(a):
    """Converteer array van strings naar woordenA.
    >>> strings2woordenA(array(['test','prYs']))
    array([[20,  5, 19, 20],
           [16, 18,  0, 19]], dtype=int8)
    """
    return letter2index[a.view((int8,a.itemsize))]

def string2woordenA(a):
    """Converteer array van strings naar woordenA.
    >>> string2woordenA('test')
    array([20,  5, 19, 20], dtype=int8)
    """
    return letter2index[array([a.encode()]).view((int8,len(a)))[0]]

#hulpfuncties----------------
def ij2Y(s):
    """Converteer de lange ij tot een interne letter.
    Houdt rekening met correcties.
    bekende uitzonderingen: minijurk, popijopie(s), strooijonker(s), ij
    >>> ij2Y('hijskraan')
    'hYskraan'
    >>> ij2Y('minijurk')
    'minijurk'
    """
    #doe geen substituut voor woorden waar de ij uit twee letters bestaat
    if s in woordenDB.correcties and woordenDB.correcties[s]=='+': return s
    return s.replace('ij','Y')

def Y2ij(s):
    """Converteer interne representatie tot "gewoon" met een ij."""
    return s.replace('Y','ij')

def callbackIterator(gen, callback, freq=250):
    """Geef de inhoud van de generator door,
    en roep iedere freq stappen de callback aan.
    Je kan de boel onderbreken door de callback een exception te laten geven.
    """
    while True:
        for i in xrange(freq): yield next(gen)
        callback()

#classes-----------------------
class WoordenDB:
    """Beheer de woordendatabase.
    correcties: de wijzigingen uit lingo-correcties.shelve
    Als je een woordenDB object hebt, kan je nog extra woorden toestaan:
    .metHoofdletters = True maakt van hoofdletters kleine letters (namen)
    .metAccenten = True laat alle accenten weg (cafe)
    .metTekens is een string van tekens om weg te laten (bijvoorbeeld ".-'")
    """
    def __init__(self, woordenlijsten=WOORDENLIJSTEN, correcties=CORRECTIES):
        self.woordenlijsten = woordenlijsten
        self.correcties = SimpleShelve(correcties, 'c')
        self.metHoofdletters = self.metAccenten = False
        self.metTekens = ''

    def readAllChunks(self, callback=None):
        """Produce contents of all word lists
        in chunks of 100000 bytes
        converts to latin1
        call callback at every chunk
        """
        #from kivy.logger import Logger
        if sys.version_info.major == 2:
            for woordenlijst in self.woordenlijsten:
                f = open(woordenlijst, 'rU')
                data = ""
                while True:
                    if callback is not None:
                        callback()
                    data += f.read(BLOCKSIZE)
                    if not data: break
                    endPos = data.rindex("\n") + 1
                    yield data[:endPos].decode('utf8').encode('latin1', 'replace')
                    data = data[endPos:]
            return
        for woordenlijst in self.woordenlijsten:
            f = open(woordenlijst, encoding='utf8')
            data = ""
            while True:
                data += f.read(BLOCKSIZE)
                if not data: break
                endPos = data.rindex("\n") + 1
                yield data[:endPos]
                data = data[endPos:]
                if callback is not None:
                    callback()

    def alleWoordenA(self, patroonOfLengte, callback=None):
        """Maak een array van woordenA met het gegeven patroon,
        of de gegeven lengte.
        Het patroon bestaat uit letters en puntjes,
        maar je mag ook gewoon een lengte opgeven.
        Als je een callback opgeeft,
        wordt deze iedere 250 woorden aangeroepen.
        >>> a = woordenA2strings(woordenDB.alleWoordenA(5))
        >>> a[:5]
        array(['Ydele', 'Ykers', 'Yking', 'Ykten', 'Ylbot'],
              dtype='|S5')
        """
        if isinstance(patroonOfLengte, int):
            lengte = patroonOfLengte
            patroon = '.'*lengte
        else:
            patroon = patroonOfLengte.replace('ij','Y')
            lengte = len(patroon)
        #controleer input
        assert set(patroon)<=set(letterlijst+'.')

        result = fromiter(self.genereerWoordenS(patroon, callback), (string_, lengte))
        #verwijder dubbele
        result = unique(result)
        #maak er een woordArray van
        return letter2index[result.view((int8, lengte))]

    def accentMap(self):
        """Make dict from letter to accented version in a string
        accented letters are latin1
        """
        import unicodedata
        result = defaultdict(str)
        for n in range(128, 256):
            try:
                descr = unicodedata.name(chr(n).decode('latin1')).split()
            except ValueError:
                continue
            if len(descr) > 5 and descr[0] == "LATIN" and descr[2] == "LETTER" and descr[4] == "WITH":
                c = descr[3]
                if len(c) > 1: continue
                if descr[1] == "SMALL": c = c.lower()
                result [c] += chr(n)
        return dict(result)

    def makeTrans(self):
        """Make translation table to remove accents
        """
        return maketrans(*map(''.join, zip(*[
            (m, c)
            for c,ml in self.accentMap().items()
            for m in ml
            ])))

    def canonicRegex(self, pat):
        """Convert pattern to match all variants of a word
        Note: there are no groups in the resulting pattern,
        which can be useful to know.
        """
        accentMap = self.accentMap()
        result = ""
        for c in pat:
            if c == '.':
                # make sure we don't match newlines or digits; rest is OK
                result += r"[^\s\d]"
            elif c == 'Y':
                result += 'ij'
            else:
                # lowercase, uppercase, or accents
                result += "[" + c + c.upper() + accentMap.get(c, '') + "]"
            result += "[-.' ]*"
        return "^" + result + "$"

    def generaliseer(self, patlist, callback=None):
        """Geef alle woorden die lijken op een woord in pat,
        zelfs als hoofdletters, accenten en alle tekens zijn toegestaan.
        """
        #TODO let op Y
        # maak enorm patroon dat alle woorden accepteert
        pat = re.compile(
            '|'.join('(' + self.canonicRegex(p) + ')'
                     for p in patlist),
            re.MULTILINE)
        for data in self.readAllChunks(callback):
            # converteer alles naar latin1
            for w in pat.finditer(data):
                yield w.group(0)

    def generiekZoekPatroon(self, patroon):
        """Genereer een reguliere expressie die alle toegelaten vormen zoekt
        van een woord, afhankelijk van instellingen.
        """
        #de ij is 1 letter
        letter = '([a-hj-z]|i(?!j)|ij)'
        return re.compile(
            # begin op het begin van een regel
                '^' +
            #een i mag niet door een j worden gevolgd
                patroon.replace('i','i(?!j)')
            #een Y mag als een ij worden gelezen
                .replace('Y', 'ij')
            #de puntjes moeten een "letter" zijn
                .replace('.', letter)
            #en eindig op het eind van een regel
                +'$', re.MULTILINE)

    def genereerWoordenS(self, patroon, callback=None):
        """Genereer alle strings uit het aangepaste woordenboek
        die aan een gegeven patroon voldoen,
        of een gegeven lengte hebben.
        Zo'n patroon mag puntjes bevatten, en daarbij is ij een letter.
        Om extra vrij lingo te spelen kan je nog dingen toestaan:
        hoofdletters: sta hoofdletters in woorden (dus: namen) toe
        accenten: sta letters met accenten toe
        tekens: sta een gegeven set tekens toe (bijvoorbeeld: "'./")
        In alle gevallen worden deze klakkeloos van de woorden afgehaald.
        >>> g = woordenDB.genereerWoordenS('q.....')
        >>> next(g)
        'quaker'
        >>> next(g)
        'quarks'
        """
        #genereer matchende woorden uit woordenboek
        matchPat = self.generiekZoekPatroon(patroon)
        makeTrans = self.makeTrans() if self.metAccenten else None
        correcties = self.correcties
        for data in self.readAllChunks(callback):
            # substituties afhankelijk van instellingen
            if self.metHoofdletters:
                data = data.lower()
            if self.metAccenten or self.metTekens:
                data = data.translate(makeTrans, self.metTekens)
            for w in matchPat.finditer(data):
                w = w.group(0)
                # verwijder correcties. Toegestane correcties komen straks
                if w not in correcties:
                    # in het woordenboek betekent ij altijd Y
                    yield w.replace('ij','Y')
        #genereer als laatste alle toegevoegde woorden
        matchPat = re.compile(patroon+'$')
        for w,erbij in correcties.items():
            if erbij=='+' and matchPat.match(w): yield w

    def kiesWoord(self, lengte):
        """Selecteer een random woord van gegeven lengte.
        Zuinig met geheugen; maakt geen array aan.
        >>> import random; random.seed(100)
        >>> woordenA2strings(woordenDB.kiesWoord(6))
        array(['hulden'],
              dtype='|S6')
        """
        resultaat = None
        for aantal,w in enumerate(self.genereerWoordenS('.'*lengte), start=1):
            #kies nieuw woord met kans 1/aantal
            if uniform(0,aantal)<1: resultaat = w
        return string2woordenA(resultaat)

    def wijzigCorrecties(self, wijzigWoorden):
        """Toon en wijzig de woordenlijst.
        Gaat er van uit dat de globale variabele correcties bestaat
        Gebruikt in het WL commando
        >>> woordenDB.wijzigCorrecties([]) #doctest: +ELLIPSIS
        Woorden verwijderd uit woordenboek:
        afdeinsde...bYectie, bYou, bYous...tropee
        Woorden toegevoegd aan woordenboek:
        bijectie, bijecties, bijou, bijous, gaarste, minijurk
        """
        letterset = set(letterlijst)
        wijzigWoorden = set(wijzigWoorden)
        correcties = self.correcties
        for w in wijzigWoorden:
            assert not set(w)-letterset, w+' is geen geldig woord'
        relevante = self.relevanteWoorden(wijzigWoorden)
        headerIsAfgedrukt = False
        for woord,inLijst in sorted(relevante.items()):
            #bepaal oude situatie, correcties gaat boven woordenlijst
            bestaat = inLijst
            if woord in correcties:
                if correcties[woord]=='+': bestaat = True
                if correcties[woord]=='-': bestaat = False
            #corrigeer
            bestaat ^= woord in wijzigWoorden
            #beschrijf wijzigingen
            changes = ''
            if bestaat==inLijst:
                #er hoeft niks veranderd te worden aan de lijst
                if woord in correcties:
                    del correcties[woord]
                    changes = 'Verwijderd uit correcties'
            elif correcties.get(woord,'')!='-+'[bestaat]:
                #er moet een entry gewijzigd worden
                correcties[woord] = '-+'[bestaat]
                changes = 'Correctie: er'+('af','bij')[bestaat]
            if changes:
                if not headerIsAfgedrukt:
                    print(" "*16+"Lijst? Corr? Wijziging:")
                    headerIsAfgedrukt = True
                print('%15s %-7s%-6s%s'%(
                    woord,
                    ('Nee','Ja')[inLijst],
                    {'':'','+':'Ja','-':'Nee'}[correcties.get(woord, '')],
                    changes))
        correcties.sync()
        self.printCorrectieOverzicht()

    def verwijderWoord(self, woord, voegToe=False):
        """Verwijder woord, simpele versie.
        Geef voegToe=True om het juist toe te voegen
        """
        letterset = set(letterlijst)
        if set(woord) - letterset:
            raise ValueError(woord)
        self.correcties[woord] = '-+'[bool(voegToe)]
        self.correcties.sync()

    def relevanteWoorden(self, wijzigWoorden):
        """Geef de woorden die relevant zijn voor de aanpassing,
        in de vorm van een dict van woord naar een vlag of hij in het
        woordenboek zit.
        Alle woorden in correcties of wijzigWoorden komen erin voor
        Hulpfunctie voor wijzigCorrecties.
        """
        #maak een dict van correcties en wijzigingen naar False
        result = dict.fromkeys(self.correcties, False)
        result.update(dict.fromkeys(wijzigWoorden, False))
        #kijk welke woorden hiervan in het woordenboek zitten
        for woordenlijst in self.woordenlijsten:
            for woord in open(woordenlijst, 'rU'):
                #newline eraf
                woord = woord[:-1]
                #namen en rare woorden mogen niet
                if not woord.isalpha() or not woord.islower(): continue
                #zet de ij klakkeloos in een Y om
                woord = woord.replace('ij','Y')
                if woord in result: result[woord] = True
        return result

    def printCorrectieOverzicht(self):
        """Hulpfunctie voor wijzigCorrecties"""
        print("Woorden verwijderd uit woordenboek:")
        print(', '.join(sorted(woord
            for woord,code in self.correcties.items()
            if code=='-')))
        print("Woorden toegevoegd aan woordenboek:")
        print(', '.join(sorted(woord
            for woord,code in self.correcties.items()
            if code=='+')))

class WoordRader:
    """Methode om een woord te raden.
    woordenA: alle woorden die aan het initiele patroon voldoen
    mogelijkA: woordenArray met alle woorden die nog mogelijk zijn
    lengte: lengte van alle woorden
    maxQ: laagste kwaliteit, plus marge; bovengrens van besteQ
    besteWoorden: lijst van (Q, woordA) met Q<maxQ
    >>> import random; random.seed(100)
    >>> import numpy.random; numpy.random.seed(100)
    >>> r = WoordRader('h.....')
    >>> r.kiesRaadWoord(r.mogelijkA)
    (array([ 8, 15,  5, 18,  5, 14], dtype=int8), 1.629258778515881, False)
    >>> r.slechtstePatroon(string2woordenA('hoeder'))
    '*...*.'
    >>> r.verwerkGok(string2woordenA('horden'), '**.***')
    >>> len(r.mogelijkA)
    4
    >>> w,q,f = r.kiesRaadWoord(r.mogelijkA)
    >>> woordenA2strings(w)[0], q
    ('holden', 1.1887218755408671)
    >>> #hier is een "verkeerd" woord raden echt nuttig: gegarandeerd goed
    >>> w,q,f = r.kiesRaadWoord()
    >>> woordenA2strings(w)[0], q
    ('hengel', 0.0)
    >>> woordenA2strings(r.mogelijkA)
    array(['honden', 'holden', 'hoeden', 'houden'],
          dtype='|S6')
    """

    def __init__(self, patroon, callback=None):
        """Bouw WoordRader van patroon.
        callback: zie alleWoordenA
        """
        #wordt gebruikt in de app om te kijken of we een nieuwe moeten maken
        self.patroon = patroon
        #je mag woorden raden uit deze set
        self.woordenA = woordenDB.alleWoordenA(patroon, callback)
        assert len(self.woordenA), "Er zijn geen woorden met dit patroon"
        shuffle(self.woordenA)
        #de woorden die nu nog kunnen
        self.mogelijkA = self.woordenA

    def kiesRaadWoord(self, bronA=None, verbose=False):
        """Kies een woord uit bronA dat bijna optimale
        kwaliteit geeft.
        Geeft 3 antwoorden:
        - een goed woordA om te raden
        - de bijbehorende kwaliteit (verwachting van onzekerheid in bits)
        - een vlag die aangeeft dat de groepsgroottes zo klein zijn
          dat je het beste een van deze woorden kan kiezen in plaats van meer
        Deze versie met niewue inzichten.
        """
        if bronA is None: bronA = self.woordenA
        t = time.clock()
        timeLimit = t+MAXTHINKTIME
        #stap 1: bereken een aantal woorden met beperkte precisie
        precisie = min(PRECISIE, len(self.mogelijkA))
        selectie = []
        goedeVolgorde = iter(self.goedeVolgorde(bronA))
        for i in goedeVolgorde:
            gokA = bronA[i]
            selectie.append((
                self.bepaalKwaliteit(gokA, precisie=precisie),
                gokA))
            if time.clock()>timeLimit: break
        selectie.sort(key=itemgetter(0))
        if verbose:
            print("Uit %d: %d*%d,"%(
                len(bronA), len(selectie), precisie), end=' ')

        if len(selectie)<AANTALBESTE<len(bronA):
            #te weinig woorden, gebruik extra tijd voor meer
            timeLimit += MAXTHINKTIME
            for i in goedeVolgorde:
                gokA = bronA[i]
                selectie.append((
                    self.bepaalKwaliteit(gokA, precisie=precisie),
                    gokA))
                if time.clock()>timeLimit: break
            selectie.sort(key=itemgetter(0))
            if verbose:
                print("%d*%d,"%(len(selectie), precisie), end=' ')
        elif precisie<len(self.mogelijkA):
            #stap 2: bereken de beste woorden met meer precisie
            #len(selectie) is een maat voor de rekensnelheid
            #gebruik dit om de precisie van stap 2 te berekenen
            precisie = PRECISIE*len(selectie)//AANTALBESTE
            precisie = min(precisie, len(self.mogelijkA))
            #maak tweede selectie met preciese kwaliteit
            selectie = [
                (self.bepaalKwaliteit(w, precisie=precisie), w)
                for k,w in selectie[:AANTALBESTE]]
            selectie.sort(key=itemgetter(0))
            if verbose:
                print("%d*%d,"%(AANTALBESTE, precisie), end=' ')

        #laatste stap: maak uitvoer
        #zoek laatste acceptabele optie
        besteKwaliteit = selectie[0][0]
        bovengrens = besteKwaliteit+INFORMATIEMARGE*precisie
        #zoek naar eerste woord dat te hoge kwaliteit heeft
        teHoog = len(selectie)
        while selectie[teHoog-1][0]>bovengrens: teHoog -=1
        if verbose:
            if besteKwaliteit<3: print('*',
            print("tijd: %.3fs; %.2f < %dw < %.2f"%(
                time.clock()-t,
                besteKwaliteit/precisie,
                teHoog,
                selectie[teHoog-1][0]/precisie)))
        #kies random woord met voldoende lage kwaliteit
        index = randrange(teHoog)
        #als besteKwaliteit < 3, zit er maar 1 2 in de counts
        #2log2 = 2.0 en 3log3 = 4.8
        #dat betekent dat je bijna zeker weet welk woord het wordt
        #en dat je dus het beste een woord uit deze set kan raden
        #zonder een raadbeurt te verliezen
        return (
            selectie[index][1],
            besteKwaliteit/precisie+log2(len(self.mogelijkA)/precisie),
            besteKwaliteit<3
            )

    def bepaalKwaliteit(self, gokA, precisie=None):
        """Bereken kwaliteit van een gok.
        Kan maar 1 gokA tegelijk verwerken wegens unique en bincount
        Precisie is een int die aangeeft hoeveel rijen van r.mogelijkA
        je gebruikt.
        Deze versie lichtelijk aangepast voor oudere versie numpy
        """
        if precisie is None: precisie = len(self.mogelijkA)
        sterA, plusA = self.bepaalRaadCodes(gokA, self.mogelijkA[:precisie])
        #zet sterA en plusA naast elkaar, en zet netjes in het geheugen
        piepCodes = concatenate((sterA,plusA),axis=1).copy()
        #maak er records van zodat unique werkt
        piepCodes = piepCodes.view([('',int8,2*gokA.shape[-1])])[:,0]
        #groepeer en tel de piepCodes. In de nieuwe numpy is dit:
        try:
            counts = bincount(unique(piepCodes, return_inverse=True)[1])
        except TypeError:
            #oude numpy: unique heeft return_inverse nog niet
            piepCodes.sort()
            comp = piepCodes[1:]!=piepCodes[:-1]
            #de oude versie van numpy geeft bij de != verkeerde shape!
            if len(comp.shape)==2: comp = comp.any(axis=1)
            idx = concatenate(([True], comp))
            u = piepCodes[idx]
            counts = bincount(searchsorted(u, piepCodes))
        #bereken Shannon waarde
        return (counts*log2(counts)).sum()

    def meting(self, bronA=None, precisie=100):
        """Meet voor verschillende waarden van precisie welke woorden
        de beste kwaliteit hebben.
        Conclusie van het onderzoek: maak een driestapsproces:
        1 goedeVolgorde om de woorden te kiezen
        2 bepaalKwaliteit met kleine precisie om uit te zoeken
        3 bepaalKwaliteit met grote precisie om definitieve woorden te kiezen
        """
        if bronA is None: bronA = self.woordenA
        outputData = []
        kolommen = []
        #bepaal perfecte lijst
        besteOpties = nsmallest(precisie, (
            (self.bepaalKwaliteit(gokA), gokA)
            for gokA in bronA),
            key=itemgetter(0))
        besteWoorden = [woordenA2strings(o[1])[0] for o in besteOpties]
        kolommen.append(["Best".ljust(len(self.patroon))] + besteWoorden[:AANTALBESTE])
        f = len(self.mogelijkA)
        kolommen.append(
            ["Kwal. "] + ["%6.2f"%(o[0]/f) for o in besteOpties[:AANTALBESTE]]
            )

        #nu met gegeven precisie: geef index in beste
        t = time.clock()
        v = self.goedeVolgorde(bronA)
        besteOpties = nsmallest(AANTALBESTE, (
            (self.bepaalKwaliteit(gokA, precisie), gokA)
            for gokA in bronA[self.goedeVolgorde(bronA)]),
            key=itemgetter(0))
        rekentijd = time.clock()-t
        kolom = ["%4d"%precisie]
        woorden = [woordenA2strings(o[1])[0] for o in besteOpties]
        for w in woorden:
            try: i = besteWoorden.index(w)
            except ValueError: i = -1
            kolom.append("%4d"%i)
        v = list(v)
        bronA = list(woordenA2strings(bronA))
        kolommen.append(kolom)
        kolommen.append(['Pos.']+[
            '%4d'%(v.index(bronA.index(w))*100//len(bronA)) for w in woorden])

        kolommen.append(['Kwal.']+
            ['%6.2f'%(self.bepaalKwaliteit(o[1])/f) for o in besteOpties])
        print('\n'.join(map(' '.join, zip(*kolommen))))
        print("Rekentijd (totaal met deze precisie):", rekentijd)

    def verwerkGok(self, gokA, piepjesS):
        "Verwerk de informatie dat gokA de gegeven piepjes oplevert"
        #ster en plus codes die bij de piepjes horen
        sterGok, plusGok = self.piepEncoder(gokA, piepjesS)
        #bepaal de ster en plus codes voor alle mogelijkheden
        sterMogl, plusMogl = self.bepaalRaadCodes(gokA)
        #zoek de goede woorden eruit
        selection = (sterGok==sterMogl).all(axis=1) \
                  & (plusGok==plusMogl).all(axis=1)
        #pas de lijst van mogelijkheden aan
        self.mogelijkA = self.mogelijkA[selection]

    def reset(self):
        "Begin opnieuw met raden vanaf het patroon"
        self.mogelijkA = self.woordenA

    def keurGokAf(self, gokA):
        "Deze gok is niet goed, registreer voor later."
        #denk aan de regel van de Morgan: any in plaats van all
        self.woordenA = self.woordenA[(self.woordenA!=gokA).any(axis=1)]
        self.mogelijkA = self.mogelijkA[
            (self.mogelijkA!=gokA).any(axis=1)]
        #zorg dat het woord in de toekomst niet meer wordt gebruikt
        woordenDB.correcties[woordenA2strings(gokA)[0]] = "-"
        woordenDB.correcties.sync()

    def bepaalRaadCodes(self, gokA, mogelijkA=None):
        """Bepaal de arrays "sterA" en "plusA" die je krijgt als je gokA
        zou raden, voor alle mogelijke woorden
        sterA bevat 1 voor "letter goed en op de goede plaats"
        plusA bevat "letter komt zo vaak voor in woord en in gokA"
        waardes in plusA komen niet boven aantal letters in gok
        >>> r = WoordRader('........')
        >>> gokA = string2woordenA('allemaal')
        >>> r.mogelijkA = strings2woordenA(array(
        ...    ['wegsmeet','aansneed','melomaan','allemaal']))
        >>> r.bepaalRaadCodes(gokA)
        (array([[0, 0, 0, 0, 1, 0, 0, 0],
                [1, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 1, 0, 1, 1, 1, 0],
                [1, 1, 1, 1, 1, 1, 1, 1]], dtype=int8),
        array([[0, 0, 0, 1, 1, 0, 0, 0],
                [2, 0, 0, 1, 0, 2, 2, 0],
                [2, 1, 1, 1, 1, 2, 2, 1],
                [3, 3, 3, 1, 1, 3, 3, 3]], dtype=int8))
        """
        if mogelijkA is None: mogelijkA = self.mogelijkA
        #bepaal sterretjes voor alle mogelijkA
        sterA = (gokA==mogelijkA).view(int8)
        #bepaal aantal keren dat letter voorkomt
        #assen: gokA, letter, gokletter
        plusA = gokA[newaxis,newaxis,:]==mogelijkA[:,:,newaxis]
        #tel aantal letters over het woord. assen: gokA, aantal per letter
        plusA = plusA.sum(axis=1)
        #sterA en plusA bepalen aantal letters op goede plaats en aantal letters
        #voorkom dat plusA groter wordt dan aantal letters in gokA
        plusA = minimum(plusA, self.telLetters(gokA)).astype(int8)
        return sterA, plusA

    def slechtstePatroon(self, gokA):
        """Geef het antwoord dat het meeste mogelijkheden geeft,
        gegeven een geraden woord.
        """
        gokA.shape = -1
        sterA, plusA = self.bepaalRaadCodes(gokA, self.mogelijkA)
        piepCodes = self.piepDecoder(gokA, sterA, plusA)
        count = Counter(map(b''.join, piepCodes))
        #most_common geeft een lijst terug met tuples(data, freq)
        return count.most_common(1)[0][0]

    @staticmethod
    def piepDecoder(woord, sterA, plusA):
        """Maak van een piepCode de piepjes in een string.
        Woord moet een array zijn (codering maakt eigenlijk niet uit)
        (Pas op: plusA wordt veranderd)
        >>> r = WoordRader('........')
        >>> woordA = string2woordenA('allemaal')
        >>> mogelijkA = strings2woordenA(array(
        ...    ['wegsmeet','aansneed','melomaan','allemaal']))
        >>> sterA,plusA = r.bepaalRaadCodes(woordA, mogelijkA)
        >>> r.piepDecoder(woordA, sterA, plusA)
        array([['.', '.', '.', '+', '*', '.', '.', '.'],
            ['*', '.', '.', '+', '.', '+', '.', '.'],
            ['.', '.', '*', '+', '*', '*', '*', '.'],
            ['*', '*', '*', '*', '*', '*', '*', '*']],
            dtype='|S1')
        """
        assert sterA.shape==plusA.shape
        #begin met sterA geschreven in . en * in plaats van 0 en 1
        piepjes = fromstring('.*',(string_,1))[sterA]
        #maak een "gewone string" van het woord, voor eersteKol
        woordString = woordenA2strings(woord)[0]
        #hou per letter de tel bij in plusA in de eerste kolom waar hij voorkomt
        plus = fromstring('+',(string_,1))
        for kolom in range(len(woord)):
            eersteKol = woordString.index(woordString[kolom])
            if eersteKol==kolom:
                #dit is de kolom waar we de tel bijhouden
                #trek 1 af voor ieder sterretje voor deze letter
                plusA[:,eersteKol] -= sterA[:, woord==woord[kolom]].sum(axis=1)
            #er moet een plusje als er nog letters zijn en als het mag
            tePlaatsen = (plusA[:,eersteKol]>0) & (sterA[:,kolom]==0)
            tePlaatsen = tePlaatsen.nonzero()[0]
            #plaats de plusjes
            piepjes[tePlaatsen, kolom] = resize(plus, tePlaatsen.shape)
            #hou de tel bij van de geplaatste plusjes
            plusA[tePlaatsen, eersteKol] -=1
        return piepjes


    @staticmethod
    def piepEncoder(woord, piepjes):
        """Vertaal een piepCode voor een gok naar een sterA en een plusA array.
        Woord moet een array zijn (codering mag S en A zijn)
        >>> WoordRader.piepEncoder(fromstring('allemaal',(string_,1)),
        ...     '*..+.+..')
        (array([1, 0, 0, 0, 0, 0, 0, 0], dtype=int8), array([2, 0, 0, 1, 0, 2, 2, 0]))
        """
        woord.shape = -1
        piepArray = fromstring(piepjes, (string_, 1))
        zelfdeLetters = woord[:,newaxis]==woord[newaxis,:]
        sterA = (piepArray==b'*').view(int8)
        plusA = ((piepArray!=b'.')&zelfdeLetters).sum(axis=1)
        return sterA,plusA

    def maakPiepjes(self, woordA, gokA):
        "Produceer piepjesS voor een gegeven gok"
        sterA,plusA = self.bepaalRaadCodes(gokA, woordA.reshape((1,-1)))
        piepjes = self.piepDecoder(gokA, sterA, plusA)
        return b''.join(piepjes[0]).decode()

    def run(self, antwoorder):
        """*Ga door totdat het woord is geraden
        antwoorder is de routine van (self, woordA)
        naar (woordA, piepjes)
        """

    @staticmethod
    def telLetters(woordA):
        """Tel van een elke letter hoe vaak hij in het woord voorkomt.
        Gaat uit van een woordArray,
        maar een array met een string erin is ook goed.
        Gebruikt door bepaalRaadCodes als bovengrens van plusA
        >>> WoordRader.telLetters(fromstring('testen',(string_,1)))
        array([2, 2, 1, 2, 2, 1], dtype=int8)
        """
        zelfde = woordA[newaxis,:]==woordA[:,newaxis]
        return zelfde.sum(axis=0, dtype=int8)

    def goedeVolgorde(self, bronA):
        """Schat in van de woorden in bronA
        hoe goed ze self.mogelijkA kunnen onderscheiden,
        om de kwaliteit te schatten.
        Geef indices in bronA terug die de volgorde aangeven
        die de kwaliteit min of meer laten stijgen (beste eerst dus)
        De correlatie is ongeveer .79
        """
        #tel de letters in mogelijkA handig (kost redelijk veel geheugen)
        #geheugengebruik voorbeeld: 1000 woorden van 7 letters kost 168K
        #dimensies: woordIndex, letterPos, letterwaarde
        groteVergelijking = self.mogelijkA[:,:,newaxis] \
                            ==arange(27)[newaxis,newaxis,:]
        #dimensies: letterPos, letterwaarde
        letterOpPos = groteVergelijking.sum(axis=0)
        #dimensies: letterwaarde
        letterInWoord = groteVergelijking.any(axis=1).sum(axis=0)
        del groteVergelijking
        letterAfwezig = maximum(0,len(self.mogelijkA)-letterInWoord)
        letterInWoord = letterInWoord-letterOpPos
        #bereken shannon waarden per positie, letter
        #van het kiezen van die letter op die positie
        #maximum(0,log2) geeft 0 als het element 0 is
        with errstate(divide='ignore'):
            letterOpPos = letterOpPos * maximum(0, log2(letterOpPos))
            letterInWoord = letterInWoord * maximum(0, log2(letterInWoord))
            letterAfwezig = letterAfwezig * maximum(0, log2(letterAfwezig))
        shannonWaarde = letterOpPos+letterInWoord+letterAfwezig
        #bepaal schatting per woord uit bronA
        letters = arange(bronA.shape[1])
        woordWaarden = shannonWaarde[letters,bronA].sum(axis=1)
        #goede woorden hebben een hoge shannon waarde
        return argsort(woordWaarden)

class Interface:
    """Een manier om met het programma te praten.
    """

    def __init__(self, patroon):
        """Zet patroon, woordlengte goed
        en initialiseer de WoordRader
        """
        self.woordlengte = len(patroon)
        self.patroon = patroon
        self.rader = WoordRader(patroon)

    def raad(self, woord):
        """Vraag aan de gebruiker wat hij van een woord vindt.
        Je mag nu de gebruiker wat leuks vertellen.
        resultaat:
            woord, piepjes.
        Woord is het woord waar je informatie over geeft (meestal gelijk aan input)
        en piepjes is een string van piepjes,
        of None voor verboden woord."""

class Mens(Interface):
    verbose = False
    def __init__(self, patroon):
        Interface.__init__(self, patroon)

    def raad(self, woordA):
        mogelijkheden = self.rader.mogelijkA
        if len(mogelijkheden)<7:
            format = "%s, " * (len(mogelijkheden)-2)
            format = "Ik twijfel tussen %s%%s en %%s" % format
            print(format % tuple(map(Y2ij, map(
                methodcaller('decode'),
                woordenA2strings(mogelijkheden)
                ))))
        else:
            print("Er zijn %d mogelijkheden: %s"%(
                len(mogelijkheden), self.matchPattern()))
        while True:
            woord = woordenA2strings(woordA)[0].decode()
            antwoord = input("Geef antwoord voor %s: "%Y2ij(woord))
            #kijk of het woord is veranderd
            if ':' in antwoord:
                nieuwwoord = antwoord[:antwoord.index(':')]
                antwoord = antwoord[antwoord.index(':')+1:]
                if len(ij2Y(nieuwwoord))!=len(ij2Y(woord)):
                    print("De lengte van het nieuwe woord is anders")
                    continue
                woord = nieuwwoord
                woordA = string2woordenA(woord)
            #en nou de piepjes...
            if antwoord == '-':
                #woord afgekeurd
                return woordA, None
            elif len(antwoord)==self.woordlengte \
            and set(antwoord)<=set('.+* =8'):
                #normaal antwoord
                antwoord = antwoord.replace(' ','.')
                antwoord = antwoord.replace('=','+')
                antwoord = antwoord.replace('8','*')
                return woordA, antwoord
            elif antwoord.islower():
                #bereken piepjes zelf bij gegeven woord
                antwoord = ij2Y(antwoord)
                if len(antwoord)==self.woordlengte:
                    piepjes = self.rader.maakPiepjes(
                        string2woordenA(antwoord), woordA)
                    break
            elif antwoord == '':
                #geef zo beroerd mogelijk antwoord
                piepjes = self.rader.slechtstePatroon(woordA)
                break
            else:
                #hulp
                print("Mogelijke antwoorden:\n"
                      "Code bestaat uit . (niks), + (goede letter), * (goede plaats)\n"
                      "Je mag ook resp. spatie, =, en 8 geven.\n"
                      "-: keur woord af\n"
                      "<woord>: genereer zelf code bij gegeven woord\n"
                      "<nieuwwoord>:<antwoord>: verander geraden woord\n"
                      "<CR>: geef code die het meeste mogelijkheden overlaat")
        print("Geef antwoord voor %s: %s"%(Y2ij(woord), piepjes))
        return woordA, piepjes

    def matchPattern(self):
        """Geef in een patroon aan wat er voor mogelijkheden zijn:
        Geef van de letters die er zeker in zitten waar ze nog kunnen,
        en voor iedere positie hoeveel letters daar nog meer kunnen.
        Dit wordt aangegeven door een cijfer dat het aantal overige letters
        aangeeft (of de letter als het er 1 is, of een * als het er meer dan 10 zijn.
        """
        #bepaal de letters die er zeker inzitten
        zekereLetters = set(range(len(letterlijst)))
        mogelijkheden = self.rader.mogelijkA
        for woord in mogelijkheden:
            zekereLetters.intersection_update(woord)

        #bepaal de mogelijke letters per positie
        letterSets = map(set, mogelijkheden.T)

        #layout
        result = []
        for letterSet in letterSets:
            #eerst de zekere letters
            mogelijkeLetters = letterSet.intersection(zekereLetters)
            formatted = b''.join(index2letter[list(mogelijkeLetters)])
            #nu de andere letters
            rest = letterSet.difference(zekereLetters)
            if len(rest)==0: pass
            elif len(rest)<=2:
                formatted += b','+b''.join(index2letter[list(rest)])
            elif len(rest)<10: formatted += str(len(rest)).encode()
            else: formatted += b'*'
            result.append(formatted.decode())
        return ' '.join(result)

class Computer(Interface):
    verbose = True
    def __init__(self, woord):
        self.woordA = string2woordenA(woord)
        woordlengte = len(woord)
        index = randrange(woordlengte)
        print("We gaan %s raden: we beginnen met de letter %s op plaats %d"%(
            Y2ij(woord), woord[index], index+1))
        patroon = '.'*index+woord[index]+'.'*(woordlengte-index-1)
        Interface.__init__(self, patroon)

    def raad(self, woordA):
        piepjes = self.rader.maakPiepjes(self.woordA, woordA)
        print("%d mogelijkheden; bij %s krijg je %s"%(
            len(self.rader.mogelijkA),
            Y2ij(woordenA2strings(woordA)[0]),
            piepjes))
        return woordA, piepjes

#hoofdroutine--------------------------
def raad(interface):
    """Ga door tot het woord geraden is, gegeven een interface.
    """
    r = interface.rader
    beurt = 0
    while len(r.mogelijkA)>1:
        antwoord = None
        while len(r.mogelijkA)>1:
            gokA, kwaliteit, stopVlag = r.kiesRaadWoord(r.mogelijkA,
                interface.verbose)
            if not stopVlag and r.mogelijkA is not r.woordenA:
                #kijk of een ruimer woord beter werkt
                gok2A, kwaliteit2, stopVlag = r.kiesRaadWoord(r.woordenA,
                    interface.verbose)
                if kwaliteit-kwaliteit2>INFORMATIEMARGE:
                    #het helpt om een fout woord te raden, dat is beter
                    gokA = gok2A
                    print("Ik ga een woord raden waarvan ik weet dat het fout is:", round(kwaliteit, 2), round(kwaliteit2, 2))
            gokA, antwoord = interface.raad(gokA)
            if not antwoord: r.keurGokAf(gokA)
            else:
                #acceptabel antwoord: ga invullen
                break
        else:
            #er is nog maar een mogelijkheid over
            break
        beurt += 1
        r.verwerkGok(gokA, antwoord)
    if len(r.mogelijkA):
        print("Het is", Y2ij(woordenA2strings(r.mogelijkA)[0].decode()))
        print("Geraden in %d beurt%s."%(beurt, beurt!=1 and "en" or ""))
    else:
        print("Ik heb geen idee; heb je het goed gedaan?")
        print("Als ik het woord niet ken, geef lingo WL <woord> om toe te voegen")

#voor test
def verzinOpgave(s):
    if len(s)>1: s=int(s[1])
    else:
        s = randrange(1000)
        print("Seed:", s)
    import random; random.seed(s)
    lengte = 6
    while True:
        w = woordenDB.kiesWoord(lengte)
        ws = woordenA2strings(w)[0].decode()
        print("Woord om te raden:", ws)
        pos = randrange(lengte)
        patroon = '.'*pos+ws[pos]+'.'*(lengte-pos-1)
        print("Gegeven:", patroon)
        r = WoordRader(patroon)
        if len(r.mogelijkA)<1000: return r
        print("Te veel mogelijkheden:", len(r.mogelijkA))

def performanceTest(s):
    #kijk welke strategie goed is
    print ("kwaliteit van woorden uit goedeVolgorde (korte moet vooraan):")
    r = verzinOpgave(s)
    for i in r.goedeVolgorde(r.mogelijkA):
        print('*' * int(r.bepaalKwaliteit(r.mogelijkA[i]) / len(r.mogelijkA)*7))
    return

#de nieuwe opties:
def selectOptions(s):
    """Kies een actie afhankelijk van de gebruikersinput.
    Gebruikersinput is sys.argv[1:], of equivalent
    Mogelijkheden ( []: optioneel )
    Woordenlijst wijzigen: WL [woord...]
    Gebruiker vult piepjes in:
        [lengte]<beginletter>: woord van gegeven lengte met beginletter
        <beginletter><lengte>: omgewisseld mag ook
        <patroon>: generalisatie: geef puntjes en letters.
    Computer doet alles zelf: cijfers, of meer dan 1 letter.
        [lengte]: verzin woord van gegeven lengte.
        <woord>: raad het gegeven woord
    """
    #Test routines
    if s[0]=='T':
        import doctest
        print(doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE))
        return
    if s[0]=='PT':
        performanceTest(s)
        return
    if s[0]=='R':
        research(s)
        return
    #Wijzigen woordenlijst
    if s[0]=="WL":
        woordenDB.wijzigCorrecties(s[1:])
        return
    assert len(s)==1, "Ik wil maar 1 argument hebben, behalve bij WL"
    s = s[0]
    #Computer gaat spelen
    if s=="": s = str(DEFAULTLENGTE)
    if s.isdigit():
        #verzin een woord als de lengte is gegeven
        s = woordenA2strings(woordenDB.kiesWoord(int(s)))[0]
        raad(Computer(s))
        return
    if len(s)>1 and s.isalpha():
        #raad een gegeven woord
        raad(Computer(ij2Y(s)))
        return
    #mensen willen zelf piepjes invoeren
    if s[0].isalpha() and s[1:].isdigit():
        #haal letter naar achteren
        s = s[1:]+s[0]
    if len(s)==1:
        #dit moet een letter zijn: voeg default lengte toe
        s = str(DEFAULTLENGTE)+s
    if s[-1].isalpha() and s[:-1].isdigit():
        #maak patroon van getal en letter
        s = s[-1]+'.'*(int(s[:-1])-1)
    #we hebben een patroon
    s = ij2Y(s)
    raad(Mens(s))

#TODO: aankondigen als zeker weet dat je het gaat weten
#TODO: de shannoninformatie aangeven
woordenDB = WoordenDB()
def research(s, verbose=False):
    """Analyseer kwaliteit onder verschillende omstandigheden"""
    l = int(s[1])
    wA = WoordRader('.'*l).woordenA
    for i in range(50):
        sw = woordenDB.kiesWoord(l)
        s = woordenA2strings(sw)[0]
        s = ij2Y(s)
        p = randrange(l)
        patroon = '.'*p+s[p]+'.'*(l-p-1)
        r = WoordRader(patroon)
        g,q,f = r.kiesRaadWoord(r.woordenA)
        #pat = r.slechtstePatroon(g)
        pat = r.maakPiepjes(sw, g)
        r.verwerkGok(g, pat)
        if verbose: print(s, patroon, woordenA2strings(g)[0], pat)
        else: print(s, patroon, woordenA2strings(g)[0], pat, end=' ')
        g1,q1,f = r.kiesRaadWoord(r.mogelijkA, verbose)
        g2,q2,f = r.kiesRaadWoord(r.woordenA, verbose)
        g3,q3,f = r.kiesRaadWoord(wA, verbose)
        print('%s %.2f  %s %.2f  %s %.2f   %.2f %.2f'%(
            woordenA2strings(g1)[0], q1,
            woordenA2strings(g2)[0], q2,
            woordenA2strings(g3)[0], q3,
            q1-q2, q1-q3))
    sys.exit(0)

def research(s):
    """Analyseer de zoekboom, met verschillende keuzes
    """
    def w2s(g): return woordenA2strings(g)[0].decode()
    patroon = s[1]
    r = WoordRader('.'*len(patroon))
    wA = r.woordenA
    r = WoordRader(patroon)
    pA = r.woordenA
    #kies genoeg woorden...
    woorden = sample(pA, 10)
    #voor drie gokken...
    gokken = nsmallest(3, (
        (pA[i], r.bepaalKwaliteit(pA[i]))
        for i in r.goedeVolgorde(pA)[:AANTALBESTE]),
        key=itemgetter(1))
    gokken = array(map(itemgetter(0), gokken))
    print('gok', ' '*max(0,len(patroon)-4),'woord piepjes')
    for gok in gokken:
        gezien = set()
        for woord in woorden:
            r.reset()
            piepjes = r.maakPiepjes(woord, gok)
            if piepjes in gezien: continue
            gezien.add(piepjes)
            print(w2s(gok), w2s(woord), piepjes)
            r.verwerkGok(gok, piepjes)
            for bron in r.mogelijkA, pA, wA:
                pairs = nsmallest(3, (
                    (bron[i], r.bepaalKwaliteit(bron[i]))
                    for i in r.goedeVolgorde(bron)[:AANTALBESTE]),
                    key=itemgetter(1))
                print(' *'[pairs[0][1]<3],'%13d'%len(bron), end=' ')
                if False:
                    for p in pairs: print(w2s(p[0]), end=' ')
                    print('%.2f %.2f'%(
                        pairs[0][1]/len(r.mogelijkA),
                        (pairs[-1][1]-pairs[0][1])/len(r.mogelijkA)))
                else:
                    print(w2s(pairs[0][0]),
                         ('%.2f'%(pairs[0][1]/len(r.mogelijkA))),
                         end=' ')
                    gok2 = pairs[0][0]
                    sterA, plusA = r.bepaalRaadCodes(gok2, r.mogelijkA)
                    piepCodes = concatenate((sterA,plusA),axis=1).copy()
                    piepCodes = piepCodes.view([('',int8,2*gok2.shape[-1])])[:,0]
                    counts = bincount(unique(piepCodes, return_inverse=True)[1])
                    print(' '.join(
                        ('%d*%d'%(v,f) if f>1 else '%d'%v)
                        for v,f in sorted(Counter(counts).items(),key=itemgetter(0))))
            if len(gezien)==3: break

if __name__=='__main__':
    if len(sys.argv)>1: selectOptions(sys.argv[1:])
    else:
        print("Gebruiker vult piepjes in:\n"
              "  [lengte]<beginletter>: woord van gegeven lengte met beginletter\n"
              "  <patroon>: generalisatie: geef puntjes en letters.\n"
              "Computer doet alles zelf: cijfers, of meer dan 1 letter.\n"
              "  [lengte]: verzin woord van gegeven lengte.\n"
              "  <woord>: raad het gegeven woord, uitgaande van beginletter\n"
              "WL <woord...>: geef lijst van afgekeurde woorden, en voeg toe/verwijder\n")
        selectOptions(input("Geef aan wat je wil: ").split())
