"""Lingo main program for android.
"""
#qpy: 2
#qpy: gui

import kivy
kivy.require('1.9.0')

from kivy.app import App
from kivy.properties import NumericProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.logger import Logger
from kivy.factory import Factory

from time import time as clock
from operator import attrgetter
import os, traceback, json
import lingo, guesser
from grid import GREY, BLUE, RED, YELLOW, WHITE, DARKRED
from grid import GuessGrid


class LingoButton(Button):
    """A character on the guessing grid
    mode determines colour: GREY, BLUE, RED, YELLOW, WHITE
    """
    mode = NumericProperty(GREY)

    def on_release(self):
        """finish click on letter
        if guessing, handle using clickGrid; otherwise, ignore
        actions for first line are handled with EntryButton:on_press
        to allow multiple selection
        """
        row, col = self.parent.child2index(self)
        Logger.debug("{:5.2f} LingoButton.on_release {}".format(
            clock() % 60,
            row, col))
        Clock.unschedule(app.calculate)
        if self.text == '?':
            self.parent.enterWord()
            return
        guessGrid = app.guessGrid
        guessGrid.clickGrid(row, col)
        if guessGrid.readyToGuess(row):
            # start guessing if ready, or at correction
            if guessGrid.currentLine == row:
                guessGrid.currentLine += 1
            Logger.info("{:5.2f} on_release: scheduling app.calculate".format(
                        clock() % 60))
            Clock.schedule_once(app.calculate,
                app.config.getfloat("intern", "delay"))


class EntryButton(LingoButton):
    """A white lingo button that allows the letter to be changed
    Slight modification of LingoButton
    """

    def on_press(self):
        """make all pressed buttons (including myself) white in the setup phase
        """
        #Logger.debug("{:5.2f} EntryButton.on_press {}".format(
        #    clock() % 60,
        #    self.parent.child2index(self)))
        if self.text != '.':
            return
        # make the pressed keys white
        for c in self.parent.topRow():
            c.mode = {'normal': GREY, 'down': WHITE}[c.state]
        # set the keyboard if applicable
        self.parent.setKeyboard()

    def on_release(self):
        """only call LingoButton.on_release if there is a guess
        """
        Logger.debug("{:5.2f} EntryButton.on_release".format(clock() % 60))
        if self.text != '.':
            super(EntryButton, self).on_release()


class ButtonGrid(GridLayout):
    """The on screen grid of buttons.
    The computations are in grid.py:GuessGrid
    """
    # accessing the cells in the grid (children are numbered backwards)
    def child2index(self, child):
        "To help the children's touch routine give row, col of letter"
        children = self.children
        index = len(children)-1-children.index(child)
        return divmod(index, self.cols)

    def index2child(self, row, col):
        """Get child with given row, col of letter
        """
        return self.children[~(row * self.cols + col)]

    def topRow(self):
        """Generate the first row of children, from left to right"""
        return reversed(self.children[~self.cols:])

    def bottomRow(self):
        """Generate the last row of children, from left to right"""
        return reversed(self.children[:self.cols])

    def updateStatus(self, status, mood):
        """Update status bar
        """
        statusLine = self.parent.ids.status
        statusLine.text = status
        # use bright colors because letters are small
        statusLine.color = [
            (.7,1,0,1,),   # HAPPY: green
            (1,1,.5,1),   # BUSY: yellow/grey
            (1,.4,.4,1),   # PROBLEM: red
            ][mood]

    # keyboard stuff
    def setKeyboard(self):
        """Turn on the keyboard.
        It will send the keys to the leftmost white cell.
        """
        Logger.debug("{:5.2f} setKeyboard".format(clock() % 60))
        if any(c.mode == WHITE for c in self.bottomRow()):
            self.parent.ids.keyboard.disabled = False
            return
        else:
            Logger.error("{:5.2f} setKeyboard called without white cells".format(
                clock() % 60))

    def releaseKeyboard(self):
        """Release the keyboard.
        """
        self.parent.ids.keyboard.disabled = True

    def on_key(self, text):
        """Check key value, then send to appropriate cell
        """
        # Flag to remember we entered the letter
        enterFlag = False
        for col, c in enumerate(self.bottomRow()):
            if c.mode != WHITE:
                # only do white letters
                continue
            elif enterFlag:
                # stay in enter mode for second letter
                return
            else:
                # change the letter
                app.guessGrid.changeLetter(-1, col, text, RED)
                enterFlag = True
        else:
            if not enterFlag:
                Logger.error("{:5.2f} on_key: no white cell".format(
                    clock() % 60))
                return
        self.releaseKeyboard()
        # word entry ended: decide between guess and newWord
        text = ''.join(map(attrgetter('text'), self.bottomRow()))
        if app.guessGrid.redLine():
            Logger.debug("{:5.2f} word entered: {}".format(clock() % 60, text))
            self.newWord()
        else:
            Logger.info(
                "{:5.2f} on_key: alerting guesser for {}".format(
                    clock() % 60,
                    text))
            app.calculate()

    def newWord(self):
        """User entered new word (or made a mistake)
        show mistake, or add word
        """
        word = ''.join(
            'Y' if c.text == 'IJ' else c.text.lower()
            for c in self.bottomRow()
            )
        Logger.info("{:5.2f} Claimed new word {}".format(
            clock() % 60,
            word))
        answersOK = True
        for row,col in app.guesser.checkResults(word):
            answersOK = False
            self.setMode(row, col, GREY)
        if answersOK:
            app.addWord(word)
            self.updateStatus("toegevoegd", guesser.BUSY)
        else:
            self.updateStatus("helaas", guesser.PROBLEM)


    # used by the guessGrid routine
    def setMode(self, row, col, mode, text=None):
        "Adjust the mode (and text) of a given letter"
        # children are numbered backwards
        #Logger.debug("{:5.2f} ButtonGrid.setMode[{},{}]={},{}".format(
        #            clock() % 60, row, col, mode, text))
        cel = self.index2child(row, col)
        cel.mode = mode
        if text is not None:
            cel.text = 'IJ' if text=='Y' else text.upper()

    def addLetters(self, letters=None):
        """Add letters to the table
        These are LingoButtons; the EntryButtons are added by setWidth
        """
        if letters is None:
            letters = '.' * self.cols
        for c in letters:
            if c=='Y': c = 'ij'
            self.add_widget(LingoButton(text=c.upper(), mode=GREY))

    def setWidth(self, width):
        """Change width of the table, and set the first line of special cells
        This routine is called from the main thread.
        """
        self.releaseKeyboard()
        if width<2:
            width = 2
        elif width>20:
            width = 20
        Logger.info("{:5.2f} restart word{}".format(
            clock() % 60,
            ('' if width == self.cols else ' of %d letters'%width)))
        self.cols = width
        self.clear_widgets()
        for i in xrange(width):
            self.add_widget(EntryButton(mode=GREY))
        Clock.schedule_once(self.initEntry, 0)

    def initEntry(self, dt):
        """callback to set the keyboard entry to the first cell
        and restart guesser
        buttonGrid must be set first
        """
        # initialise guesser
        app.guesser.message = "restart {:d}".format(self.cols)
        c = self.children[-1]
        c.mode = WHITE
        self.setKeyboard()

    # called by children
    def enterWord(self):
        """Start entering new word
        by filling question marks
        """
        Logger.debug("{:5.2f} start entering word".format(clock() % 60))
        # make all question marks white and activate keyboard
        self.updateStatus("Voer in", guesser.HAPPY)
        for c in self.bottomRow():
            if c.text == '?': c.mode = WHITE
        self.setKeyboard()

    # called by Kivy
    def justGuessed(self):
        """Determine if there is a recent guess on the board.
        This determines when you can ask to change the guess.
        This occurs when there are
        grey or dark red, but no white tiles
        """
        colors = {c.mode for c in self.children}
        if WHITE in colors:
            return False
        # not if the word is unknown
        if any(c.text == '?' for c in self.bottomRow()):
            return False
        return bool({GREY, DARKRED} & colors)


class RootWidget(BoxLayout):
    pass


class SettingsWidget(BoxLayout):
    __events__ = ('on_close', )
    def on_close(self, *args):
         pass


class LingoApp(App):
    def build(self):
        rootWidget = RootWidget()
        # buttonGrid is last child of root widget
        self.buttonGrid = rootWidget.ids.buttonGrid
        self.thoughts = rootWidget.ids.thoughts
        self.myKeyboard = rootWidget.ids.keyboard

        self.guessGrid = GuessGrid(self.buttonGrid)

        self.guesser = guesser.Guesser(
            callback=self.guesserCallback,
            config=self.config)
        # start background process
        self.guesser.start()

        # only for the back button
        Window.bind(on_keyboard=self.catchEscape)
        return rootWidget

    def catchEscape(self, game, keycode, modifiers, string, *args):
        """General key catcher that is only used to detect if someone
        types the Android back button
        In this program, apperently only called like this:
                       keycode modifiers string
        new keyboard?  113     45         u'\x04'
        text change?   32      32         u''
        back button    27      4          u''    only in setWidth case
        """
        signature = keycode, modifiers, string
        Logger.debug("{:5.2f} LingoApp.catchEscape {}".format(clock() % 60, signature))
        # there are only three known patterns
        if signature == (113, 45, '\x04'):
            pass
        elif signature == (32, 32, ''):
            pass
        elif signature == (27, 4, ''):
            # escape without keyboard: clear
            Logger.info("{:5.2f} Back button detected".format(
                clock () % 60))
            if self.buttonGrid.children[-1].mode == WHITE:
                self.stop()
            self.guessGrid.setWidth(self.buttonGrid.cols)
        else:
            Logger.warning("{:5.2f} unknown keyboard signature")
        return True

    def build_config(self, config):
        """Set defaults if needed. Called by Kivy"""
        config.adddefaultsection("spel")
        config.setdefaults("spel", {
            "lengte": 5,
            "denktijd": .5,
            "raden": "vaak",
            "gegeven": 1,
            "toestaan": ""})
        config.adddefaultsection("intern")
        config.setdefaults("intern", {
            "delay": .6,
            "statusupdate": .2,
            "sample": 150,
            "attempts": 30,
            "loss": 0.3})
        config.adddefaultsection("dictionaries")
        config.setdefaults("dictionaries", {
            "folder": "wordlists",
            "files":
                json.dumps([os.path.basename(f)
                    for f in lingo.WOORDENLIJSTEN]),
            "exceptions": "exceptions.shelve"})

    def get_application_config(self):
        return os.path.join(lingo.SCRIPTDIR, 'lingo.ini')

    def open_settings(self, *args):
        super(LingoApp, self).open_settings(*args)

    def create_settings(self):
        "Maak mijn eigen settings page"
        s = SettingsWidget()
        s.bind(on_close=self.close_settings)
        return s

    def close_settings(self, *args):
        Logger.debug("{:5.2f} closing settings".format(
            clock () % 60))
        self.config.write()
        self.guesser.message = "update"
        # return the value True from parent class
        return super(LingoApp, self).close_settings()

    def on_pause(self):
        self.config.write()
        return True

    def on_resume(self):
        pass

    def on_stop(self):
        Logger.info("{:5.2f} Stopping guesser".format(
            clock() %60))
        try: self.guesser.shutdown()
        except:
            Logger.error("While finishing:" + traceback.format_exc())
        pass

    # custom
    def guesserCallback(self, mode):
        """guesser calls back to tell us there is new information to read
        mode indicates value to read
        """
        if mode == "reply":
            reply, onlyWord = self.guesser.reply
            if len(reply) == self.buttonGrid.cols:
                # only apply if length still the same
                self.guessGrid.putWord(reply, onlyWord)
                Logger.info(self.guesser.thoughts.replace("\n", ";"))
        elif mode == "status":
            status, mood = self.guesser.status
            self.buttonGrid.updateStatus(status, mood)
        elif mode == "thoughts":
            thoughts = self.guesser.thoughts
            self.thoughts.text = thoughts

    def calculate(self, dt=None):
        Logger.debug("{:5.2f} LingoApp.calculate".format(clock() % 60))
        self.guesser.message = self.guessGrid.data

    def editDicts(self, d=dict.fromkeys("abc", False)):
        Logger.warning("{:5.2f} editDicts".format(
            clock() % 60))
        p = Factory.SelectList()
        p.title = "Gebruikte woordenboeken"
        addWidget = p.ids.scrolled.add_widget
        checkbox = Factory.ScrolledCheckBox
        selected = self.config.get("dictionaries", "files")
        for f in os.listdir("wordlists"):
            addWidget(checkbox(
                text=f,
                state=["normal", "down"][f in selected]
                ))
        def sync(p):
            boxes = p.ids.scrolled.children
            result = [b.text
                      for b in boxes
                      if b.state == 'down']
            self.config.set("dictionaries",
                            "files",
                            json.dumps(result))
        self.guesser.message = "update"
        p.bind(on_dismiss=sync)
        p.open()

    def deleteWord(self, temporary):
        lastWord = ''.join(self.guessGrid.data[-1]['letter'])
        Logger.warning("{:5.2f} deleteWord {}, temporary:{}".format(
            clock() % 60,
            lastWord,
            temporary))
        if not temporary:
            lingo.woordenDB.verwijderWoord(lastWord)
            self.guesser.message = "update"
        self.guesser.message = "forget"
        self.buttonGrid.updateStatus("vergeten", guesser.HAPPY)

    def addWord(self, word):
        """add word to dictionary
        """
        Logger.warning("{:5.2f} adding word {}".format(
            clock() % 60,
            word))
        lingo.woordenDB.verwijderWoord(word, voegToe=True)
        self.guesser.message = "update"

    def showThoughts(self):
        Logger.warning("showThoughts not implemented")

    def editWords(self):
        p = Factory.SelectList()
        p.title = "Aanpassingen aan woordenboek (Y=ij)"
        checkbox = Factory.ScrolledCheckBox
        addWidget = p.ids.scrolled.add_widget
        for w,f in sorted(lingo.woordenDB.correcties.items()):
            addWidget(checkbox(
                text=w,
                state={'-': "normal", '+':"down"}[f]))
        def sync(p):
            correcties = lingo.woordenDB.correcties
            for b in p.ids.scrolled.children:
                correcties[b.text] = "-+"[b.state == 'down']
            correcties.sync()
            self.guesser.message = 'correct'
        p.bind(on_dismiss=sync)
        p.open()

    def help(self):
        p = Factory.HelpWindow()
        p.open()


if __name__ == '__main__':
    kivy.resources.resource_add_path('icons')
    try:
        # keep a link to the app
        app = LingoApp()
        app.run()
    except:
        Logger.error("While running:\n" + traceback.format_exc())

