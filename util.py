#!python3
"""Utility functions for lingo programma
"""
from __future__ import print_function
import sys
from functools import wraps
if sys.version_info.major < 3:
    input = raw_input

ENABLEDEBUG = True

try: from kivy.logger import Logger
except ImportError: ENABLEDEBUG = False

from itertools import starmap
from traceback import extract_stack, format_exc
from functools import wraps
from os.path import basename
from time import time as clock
import re
from collections import defaultdict

if ENABLEDEBUG:
# debug mode: activate @debug decorator
    def debug(f):
        "Decorator printing input and output of a function"
        @wraps(f)
        def wrapper(*args, **kwds):
            filename, lineno, functionname, text = extract_stack(limit=2)[0]
            argdescr = ', '.join(map(repr, args)+list(starmap(
                '{}={!r}'.format, kwds.items())))
            Logger.info("{:5.2f} Calling {}({}) from {}, line {:d}, in {}"
                    .format(
                clock()%60,
                f.__name__,
                argdescr,
                basename(filename),
                lineno,
                functionname))
            try: result = f(*args, **kwds)
            except Exception as e:
                Logger.info("Running {}({}) raised {}".format(
                    f.__name__, argdescr, e))
                Logger.info(format_exc())
                raise
            Logger.info("{:5.2f} Returning {}({}) = {!r}".format(
                clock()%60,
                f.__name__,
                argdescr,
                result))
            return result
        return wrapper

else:
# dummy versies van debug en Logger, zodat je de Logger kan laten staan in de code
    def debug(f):
        "dummy decorator"
        return f
    class Logger:
        "dummy logger"
        @staticmethod
        def trace(message): print("trace:", message)
        @staticmethod
        def debug(message): print("debug:", message)
        @staticmethod
        def info(message): print("info:", message)
        @staticmethod
        def warning(message): print("warning:", message)
        @staticmethod
        def error(message): print("error:", message)
        @staticmethod
        def critical(message): print("critical:", message)

def replaceAll(old):
    FILES = [f
             for f in os.listdir(".")
             if f.endswith(".py") or f.endswith(".kv")]
    total = 0
    for f in FILES:
        seen = False
        for lineno,line in enumerate(open(f)):
            if re.search(old, line):
                if not seen: print("---File:", f)
                print(lineno+1, end=": " + line)
                total = seen = True
    if not total:
        return use
    new = input("with (empty=cancel):")
    if not new:
        print("Cancelled")
        return
    for f in FILES:
        c = open(f).read()
        open(f + '.bak', 'w').write(c)
        open(f, 'w').write(re.sub(old, new, c))

def compare(name):
    options = []
    for d in os.listdir('..'):
        if os.path.exists(os.path.join('..', d, name)):
            options.append(d)
            print(len(options), d)
    if not options: return
    if len(options) > 1:
        d = options[int(input("Welke? ")) - 1]
    else:
        d = 0
    other = os.path.join('..', d, name)
    import difflib
    for line in difflib.unified_diff(
            open(other).readlines(),
            open(name).readlines(),
            other,
            name,
            n=5):
        print(end=line)

class Aff:
    """Contains the collected information of an affix file
    compoundMin: COMPOUNDMIN
    flagType: eascii or what FLAG gives
    compoundRules: COMPOUNDRULE (list)
    prefixes/suffixes: dict
    flag values (None if not defined):
        forbiddenWord: FORBIDDENWORD
        onlyInCompound: ONLYINCOMPOUND
        keepCase: KEEPCASE
        compoundBegin: COMPOUNDBEGIN
        compoundMiddle: COMPOUNDMIDDLE
        compoundEnd: COMPOUNDEND
        compoundPermit: COMPOUNDPERMITFLAG
        checkCompoundDup: indicates presence of CHECKCOMPOUNDDUP
        checkCompoundPat: list of CHECKCOMPOUNDPATTERN triples
    """
    def __init__(self, name):
        self.compoundMin = 3
        self.flagType = 'eascii'
        self.compoundRules = []
        self.prefixes = defaultdict(list)
        self.suffixes = defaultdict(list)
        self.iterator = self.lineIter(name)
        self.forbiddenWord = self.onlyInCompound = self.keepCase = None
        self.compoundBegin = self.compoundMiddle = self.compoundEnd = None
        self.compoundPermit = None
        self.checkCompoundDup = False
        self.checkCompoundPat = []
        for command, *args in self.iterator:
            getattr(self, command)(*args)
        self.show()

    def lineIter(self, name):
        f = open(name, encoding='utf8')
        # skip BOM
        if f.read(1) != '\ufeff':
            f.seek(0)
        try:
            while True:
                line = next(f).rstrip()
                if not line: continue
                if line.startswith('#'): continue
                if " C0" in line: print(line)
                yield line.split()
        except StopIteration: pass

    def show(self):
        print('Number of prefix flags:', len(self.prefixes))
        print('Prefix flags:', ''.join(sorted(self.prefixes)))
        print('Number of prefix conditions:', len(set(
            cond
            for l in self.prefixes.values()
            for cond, s1, s2 in l)))
        print('Number of different prefixes:', len(set(
            s2
            for l in self.prefixes.values()
            for cond, s1, s2 in l)))
        print('Number of suffix flags:', len(self.suffixes))
        print('Suffix flags:', ''.join(sorted(self.suffixes)))
        print('Number of suffix conditions:', len(set(
            cond
            for l in self.suffixes.values()
            for cond, s1, s2 in l)))
        print('Number of different suffixes:', len(set(
            s2
            for l in self.suffixes.values()
            for cond, s1, s2 in l)))
        print('Highest number of options in a suffix:',
            max(map(len, self.suffixes.values())))
        for f in "compoundMin flagType forbiddenWord onlyInCompound keepCase compoundBegin compoundMiddle compoundEnd compoundPermit checkCompoundDup".split():
            v = getattr(self, f)
            if v is not None: print ("{}: {}".format (f, v))

    def multiline(nargs):
        """for rules that take multiple lines
        just define the line rule
        probably the most unreadable choice i've written
        """
        def m(f):
            "function doing the first pass substitution"
            name = f.__name__
            #@wraps(f)
            def p(self, *args):
                "the replacement function"
                if len(args) != nargs:
                    raise TypeError("{} expects {} args, not {}".format(
                        name,
                        nargs,
                        len(args)))
                for i in range(int(args[-1])):
                    command, *args = next(self.iterator)
                    if command != name: raise ValueError
                    try: f(self, *args)
                    except TypeError:
                        # fix syntax error in NL file
                        args = args[:f.__code__.co_argcount - 1]
                        if f.__defaults__ and args[-1].startswith('#'):
                            # yet another hack to ignore syntax errors in nl_NL
                            args = args[:-1]
                        try: f(self, *args)
                        except TypeError:
                            print('Syntax error:', ' '.join((command, *args)))
            return p
        # handle @multiline and @multiline(3)
        if isinstance(nargs, int):
            return m
        # no parameter, substitute 1
        f = nargs
        nargs = 1
        return m(f)

    def SET(self, encoding):
        if not encoding == "UTF-8":
            raise ValueError(encoding)

    def TRY(self, chars):
        "characters to try for replacement"

    @multiline
    def ICONV(self, s1, s2):
        "input conversion"

    def NOSUGGEST(self, flag):
        "don't suggest"

    def COMPOUNDMIN(self, n):
        "min length of compound parts"
        self.compoundMin = int(n)

    def ONLYINCOMPOUND(self, flag):
        self.onlyInCompound = flag

    @multiline
    def COMPOUNDRULE(self, rule):
        self.compoundRules.append(rule)

    def WORDCHARS(self, chars):
        "extra characters allowed in words"

    @multiline
    def REP(self, s1, s2):
        "preferred typo replace"

    @multiline(3)
    def PFX(self, flag, s1, s2, condition='.'):
        if s1 == '0': s1 = ''
        self.prefixes[flag].append((condition, s1, s2))

    @multiline(3)
    def SFX(self, flag, s1, s2, condition='.'):
        if s1 == '0': s1 = ''
        # fix NL bug
        if condition == '.': condition = ''
        if condition.endswith(s1): condition = s1
        if not s1.endswith(condition):
            print("invalid suffix", flag)
        self.suffixes[flag].append((condition, s1, s2))

    def KEY(self, arg):
        "Keyboard layout"

    def NOSPLITSUGS(self):
        "Don't suggest spaces in words"

    def KEEPCASE(self, flag):
        "Forbid uppercased and lowercased forms"
        self.keepCase = flag

    @multiline
    def BREAK(self, chars):
        "For hyphenation"

    def FLAG(self, flagType):
        self.flagType = flagType

    def FORBIDDENWORD(self, flag):
        self.forbiddenWord = flag

    @multiline
    def MAP(self, seqs):
        "Related sequences"

    @multiline
    def OCONV(self, s1, s2):
        "Output conversion table"

    def CHECKCOMPOUNDCASE(self):
        "Forbid upper case at word boundaries"

    def COMPOUNDBEGIN(self, flag):
        self.compoundBegin = flag

    def COMPOUNDMIDDLE(self, flag):
        self.compoundMiddle = flag

    def COMPOUNDEND(self, flag):
        self.compoundEnd = flag

    def COMPOUNDPERMITFLAG(self, flag):
        self.compoundPermit = flag

    def CHECKCOMPOUNDDUP(self):
        "Forbid word duplication"
        self.checkCompoundDup = True

    @multiline
    def CHECKCOMPOUNDPATTERN(self, endchars, beginchars, repl=''):
        "Forbid compounding these patterns"
        self.checkCompoundPat.append((endchars, beginchars, repl))

def huntest():
    # Dutch file is horrible: in line "comments",
    # . for pattern is s1 is given,
    # undefined patterns that are in duct
    Aff("hunspell/nl_NL.aff")
    #Aff("hunspell/en_GB.aff")
    from collections import Counter
    c = Counter()
    for line in open("hunspell/nl_NL.dic", encoding="utf8"):
        p = line.rfind('/')
        if p > 0 and line[p-1] != '\\':
            c.update(map(''.join, zip(*[iter(line[p+1:])]*2)))
    from pprint import pprint
    pprint(c)

def checkDemo(woord="beneden"):
    print("woord:", woord)
    import lingo
    woordA = lingo.string2woordenA(woord)
    wr = lingo.WoordRader('.' * len(woord))
    bruikbaar = 0
    for wA in wr.woordenA:
        w = lingo.woordenA2strings(wA)[0]
        piepPerLetter = dict.fromkeys(w, '')
        if len(piepPerLetter) == len(woord): continue
        sA,pA = wr.bepaalRaadCodes(wA, woordA.reshape((1, -1)))
        piepjes = b''.join(wr.piepDecoder(wA, sA, pA)).decode()
        for letter,piep in zip(w, piepjes):
            piepPerLetter[letter] += piep
        if any(
                '.' not in ps or '+' not in ps
                for ps in piepPerLetter.values()
                if len(ps) > 1):
                continue
        bruikbaar += 1
        p = input("Voor " + w.decode() + ":")
        r = list(doCheck(w, piepjes, p))
        if r: print(piepjes, r)
        if bruikbaar >= 10: break

def doCheck(woord, piepjes, antwoord):
    # per letter wat eet verwacht wordt, een waar
    verwachtPerLetter = {}
    for pos,(w, p, a) in enumerate(zip(woord, piepjes, antwoord), start=1):
        if p == a:
            continue
        if p == '*' or a == '*':
            yield pos
            continue
        if w not in verwachtPerLetter:
            verwachtPerLetter[w] = [p]
        verwachtPerLetterW = verwachtPerLetter[w]
        if verwachtPerLetterW[0] == p:
            verwachtPerLetterW.append(pos)
        else:
            verwachtPerLetterW.pop()
            if len(verwachtPerLetterW) == 1:
                del verwachtPerLetter[w]
    for posL in verwachtPerLetter.values():
        for pos in posL[1:]:
            yield pos


if __name__ == "__main__":
    import os, re
    os.chdir(os.path.dirname(__file__) or '.')

    # compare("main.py")

    # old = r'\b' + input("Replace:") + r'\b'
    # replaceAll(old)
    checkDemo("eidereend")
