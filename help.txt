[size=24][b][size=30]Spelregels[/size][/b]

Lingo is een spelletje op TV waarbij
de kandidaten woorden moeten raden.
Aan het begin van elke ronde is er een
letter gegeven en de lengte van het
woord.
De kandidaten graden woorden van het
zelfde aantal letters.

Als terugkoppeling krijgen ze
informatie over welke letters juist
zijn en welke letters op de goede
plaats staan.
Letters die in het woord voorkomen
worden gemarkeerd met een gele cirkel
en letters die op de juiste plaats in
het woord staan worden met een rood
vierkant aangegeven.
De overige letters hebben een blauwe
achtergrond.
Als alle letters een rood vierkant
hebben is het woord geraden.

Dit programma is ontstaan om te zien
hoe lang je erover zou moeten doen om
een woord te raden.



In ons geval verzin je het woord zelf
en gaat je telefoon of computer het
woord raden.
Je kan zelf bedenken hoeveel letters
van het woord je van tevoren geeft.
Er wordt dan door middel van een
geavanceerd algoritme geprobeerd het
woord zo snel mogelijk te raden.



[b][size=30]Bediening[/size][/b]

Aan het begin bepaal je welke letters
je als ge geeft gaat beschouwen.
Dit wordt aangegeven door een wit
vierkant.
Aanvankelijk is dit de eerste letter.
Je kan een ander vierkant kiezen door
er op te tikken.
Je kan ook meerdere vierkanten
selecteren door er tegelijk op te
tikken.

Daarna kies je de gegeven letters, van
links naar rechts, met het
toetsenbord.

De computer begint het eerste woord te
raden.
Je geeft zelf aan welke letters er
goed zijn door erop te tikken.
Een keer tikken maakt de letter blauw,
twee keer tegen maakt hem rood, en
drie keer maakt de letter geel.
Als je je vergist kan je gewoon nog
een keer tikken om de letter weer in
dezelfde cyclus blauw, rood en geel te
maken.



[b][size=30]Instellingen[/size][/b]

De lengte van het te raden woord kan
met de “+” en “–”knop er bovenaan
worden ingesteld.
Er is nog een heleboel meer aan dit
programma in te stellen.

Er zijn instellingen die te maken
hebben met de spelregels en
instellingen die te maken hebben met
het raadalgoritme; deze worden
onderaan de tekst toegelicht.
Het instellingenscherm wordt
geactiveerd op het konijntje links
boven te drukken.



[b]Instellingen van de spelregels[/b]

Als eerste kan je de beginwaarde van
de woordlengte instellen.

Als tweede kan je de tijd die computer
mag nadenken instellen.

Als de computer detecteert dat het
efficiënter is om woorden te raden die
geen oplossing kunnen zijn zal hij dat
doen.
Mensen hebben de neiging om steeds
woorden te raden die een mogelijke
oplossing zijn.
Je kan bepalen hoe vaak dit gebeurt.

Vervolgens kan je verbieden om woorden
te gaan die niet aan de gegeven letter
voldoen.



De twee knoppen maken het mogelijk om
de gebruikte woordenboeken te kiezen
een ook individuele uitzonderingen te
maken.
Dit heeft onder andere met de letter
"ij" te maken: het programma gaat er
van uit dat als een "i" en "j" achter
elkaar in een worden in het
woordenboek staan, het de letter "ij"
betekent.
Je kan uitzonderingen hierop (zoals
minijurk of bijectie) als
uitzonderingen instellen.



De overige instellingen bepalen het
raadalgoritme en worden onderaan
toegelicht, na deze uitleg over het
algoritme.



[b][size=30]Algoritme[/size][/b]


Dit programma probeert het woord te
raden dat een optimale hoeveelheid
informatie geeft over het te raden
woord.
Dit houdt in dat het voor ieder
mogelijk woord dat het zou kunnen
zijn, en voor ieder woord dat geraden
kan worden, uitrekent wat het antwoord
zou worden.
Door nu een woord te kiezen waarvan de
mogelijke antwoorden zo veel en zo
verschillend mogelijk zijn, wordt er
informatie verkregen over het te raden
woord.

Als er nog twee of drie woorden over
zijn, wordt een willekeurig woord
geraden dat het zou kunnen zijn, omdat
het dan niet meer uitmaakt.
Als het aantal 4 of meer is, wordt ook
overwogen om een woord te raden dat
geen oplossing is, als dit meer
informatie oplevert.
Als er nog één woord over is, worden
de onbekende letters donkerrood
gemaakt in plaats van grijs.
Als er helemaal geen woord mogelijk
is, worden er vraagtekens neergezet.

Soms heeft het zelfs zin om een woord
te raden dat niet aan de beginletter
voldoet. Je kan instellen hoe vaak dit
mag voorkomen.



[b][size=30]Optimalisaties[/size][/b]

Een van de interessantste
eigenschappen van het algoritme is dat
het voor een computer eigenlijk
onmogelijk is om al deze combinaties
in korte tijd uit te proberen.
Om dit toch zo snel voor elkaar te
krijgen, worden de volgende
tussenstappen gedaan:

Eerst wordt van alle mogelijke woorden
een steekproef genomen om het aantal
mogelijkheden te beperken.

Van de woorden in deze steekproef
wordt een schatting berekend van hoe
goed ze zijn.
Uit de beste woorden wordt een
beperkte selectie gemaakt, beperkt door
de rekentijd.

Daarna wordt van deze woorden precies
berekend hoe goed ze zijn.

Vervolgens wordt uit de beste woorden
een willekeurig woord gekozen dat
bijna goed is.



[b][size=30]Instellingen van het raadalgoritme[/size][/b]

De eerste instelling van het
raadalgoritme bepaalt hoe lang de
computer wacht voordat hij begint te
raden.
Dit voorkomt dat hij begint te raden
voordat je klaar bent met het invullen
van de terugkoppeling.

Daarna kies je de grootte van de
steekproef van woorden die precies
worden uitgerekend.

Vervolgens kies je het aantal woorden
dat precies wordt doorgerekend.

Als laatste kan je kiezen hoe precies
voor het beste woorden wordt gekozen.
Als je deze marge groter kiest, wordt
het raden minder saai, maar zal hij er
soms een beurt langer over doen.
